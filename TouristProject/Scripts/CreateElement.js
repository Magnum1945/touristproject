﻿function CreatePlaceDiv(idName, className, divId, innerHtml) {
    var div = document.createElement('div');
    div.setAttribute("id", idName);
    div.setAttribute("class", className);
    div.setAttribute("data-id", divId);
    div.innerHTML = innerHtml;
    return div;
}

function CreateTripDiv(idName, className, divId, divTime, innerHtml) {
    var div = document.createElement('div');
    div.setAttribute("id", idName);
    div.setAttribute("class", className);
    div.setAttribute("data-id", divId);
    div.setAttribute("data-time", divTime);
    div.innerHTML = innerHtml;
    return div;
}

function CreateCourseDiv(idName, className, divDistance, divDuration, innerHtml) {
    var div = document.createElement('div');
    div.setAttribute("id", idName);
    div.setAttribute("class", className);
    div.setAttribute("data-distance", divDistance);
    div.setAttribute("data-duration", divDuration);
    div.innerHTML = innerHtml;
    return div;
}

function CreateImg(idName, className, srcName, altName) {
    var img = document.createElement('img');
    img.setAttribute("id", idName);
    img.setAttribute("class", className);
    img.setAttribute("src", srcName);
    img.setAttribute("alt", altName);
    return img;
}

function CreateA(idName, className, hrefName, innerHtml) {
    var link = document.createElement('a');
    link.setAttribute("id", idName);
    link.setAttribute("class", className);
    link.setAttribute("href", hrefName);
    link.setAttribute("target", "_blank");
    link.innerHTML = innerHtml;
    return link;
}