﻿function DragAndDrop() {
    $(function DragAndDrop() {
        $("div.gallery_one").sortable({
            connectWith: "div"
        });

        $("div.gallery_two").sortable({
            connectWith: "div"
        });

        $("#gallery-one, #gallery-two").disableSelection();
    });
}