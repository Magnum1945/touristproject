﻿function InitMap(mapDivId) {
    var mapOptions = {
        zoom: 14,
        panControl: true,
        zoomControl: true,
        mapTypeControl: true,
        scaleControl: true,
        streetViewControl: true,
        overviewMapControl: true,
        rotateControl: true,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById(mapDivId), mapOptions);
    return map;
}

function CalculateAndDisplayRoute(directionsService, directionsDisplay, markerList, urlSendJsonRoutes) {
    var wayPoints = [];
    var html;
    for (var i = 0; i < markerList.length; i++) {
        if (i !== 0 & i !== markerList.length - 1) {
            wayPoints.push(
                {
                    location: markerList[i],
                    stopover: true
                });
        }
    }

    var request = {
        origin: markerList[0],
        destination: markerList[markerList.length - 1],
        waypoints: wayPoints,
        optimizeWaypoints: true,
        travelMode: google.maps.TravelMode.WALKING
    };

    directionsService.route(request,
        function GetResponse(response, status) {
            if (status === google.maps.DirectionsStatus.OK) {
                directionsDisplay.setDirections(response);
                SendRoutesToServer(response, urlSendJsonRoutes);
            }
            else {
                window.alert('Directions request failed due to ' + status);
            }
        });
}

function GetPointList(urlGetPointList) {
    var pointList;
    $.ajax({
        type: "POST",
        url: urlGetPointList,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            pointList = data;
        },
        async: false,
        failure: function (errMsg) {
            alert(errMsg);
        }
    });
    return pointList;
}

function GetMarkerList(pointList) {
    var markerList = [];
    for (var i = 0; i < pointList.length; i++) {
        markerList.push(new google.maps.LatLng(pointList[i]["Latitude"], pointList[i]["Longitude"]));
    }
    return markerList;
}

function SendRoutesToServer(jsonObject, urlSendJsonRoutes) {
    $.ajax({
        url: urlSendJsonRoutes,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify(jsonObject)
    });
}

function ShowPointArray(map, pointList) {
    var markerList = GetMarkerList(pointList);
    map.setCenter(markerList[0]);
    for (var i = 0; i < markerList.length; i++) {
        marker = new google.maps.Marker(
            {
                map: map,
                position: markerList[i],
            });
    }
}
