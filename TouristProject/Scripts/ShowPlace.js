﻿function initialize() {
    var id = GetPlaceId();
    var point = GetPlace("/Trip/GetPlace?id=" + id);
    var pointList = [point];
    var map = InitMap("placeMap");
    ShowPointArray(map, pointList);
}

function GetPlaceId() {
    var elements = document.getElementById("placePhoto");
    var id = elements.getAttribute("data-id");
    return id;
};

function GetPlace(urlGetPlace) {
    var point;
    $.ajax({
        type: "POST",
        url: urlGetPlace,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            point = data;
        },
        async: false,
        failure: function (errMsg) {
            alert(errMsg);
        }
    });
    return point;
}