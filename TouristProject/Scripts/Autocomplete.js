﻿function AutoComplete(divId, urlAutocomplete, isCallback, urlSelected) {
    $(divId).autocomplete({
        source: function (request, response) {
            $.ajax({
                url: urlAutocomplete,
                type: "POST",
                dataType: "json",
                data: { Prefix: request.term },
                success: function (data) {
                    response($.map(data, function (item) {
                        return { label: item.Name, value: item.Id };
                    }));
                }
            });
        },
        select: function (event, ui) {
            if (isCallback === true) {
                $.ajax({
                    url: urlSelected,
                    type: "POST",
                    dataType: "json",
                    data: JSON.stringify(ui.item.Id)
                });
            }
        },
        messages: {
            noResults: "", results: ""
        }
    });
}
