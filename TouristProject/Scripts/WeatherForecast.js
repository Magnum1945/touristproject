﻿function GenerateForecast() {
    $("#sendCity").click(function () {
        var cityName = $("#forecastCity").val();
        if (cityName.value == "") {
            alert("Please enter city first!");
        }
        else {
            $('#weatherForecast').empty();
            var forecast = GetForecast(cityName);
            for (var i = 0; i < forecast.list.length; i++) {
                ShowForecast(forecast.list[i]);
            }
        }
    });
}

function GetForecast(cityName) {
    var forecast;
    $.ajax({
        type: "POST",
        url: "/Trip/GetForecast?city=" + cityName,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            forecast = data;
        },
        async: false,
        failure: function (errMsg) {
            alert(errMsg);
        }
    });
    return forecast;
}

function ShowForecast(forecast) {
    var main = $('#weatherForecast');
    var dayForecast = $('<div>').attr({ class: "dayForecast" });
    var date = ConvertDate(forecast.dt);
    var dateTime = $('<div>').attr({ class: "weatherDate" }).text(date);
    var icon = $('<div>').attr({ class: "weatherIcon" });
    var img = $('<img id="weatherImage">').attr('src', "http://openweathermap.org/img/w/" + forecast.weather[0].icon + ".png");
    icon.append(img);
    var description = $('<div>').attr({ class: "weatherDescription" }).text(forecast.weather[0].description);
    var day = $('<div>').attr({ class: "weatherForecastDay" }).text("Day - " + forecast.temp.day + " °C");
    var night = $('<div>').attr({ class: "weatherForecastNight" }).text("Night - " + forecast.temp.night + " °C");
    var wind = $('<div>').attr({ class: "weatherForecastWind" }).text("Wind speed - " + forecast.speed + " m/s");
    var pressure = $('<div>').attr({ class: "weatherForecastPressure" }).text("Atmospheric pressure - " + forecast.pressure + " hpa");
    var humidity = $('<div>').attr({ class: "weatherForecastHumidity" }).text("Humidity - " + forecast.humidity + " %");
    dayForecast.append(dateTime);
    dayForecast.append(icon);
    dayForecast.append(description);
    dayForecast.append(day);
    dayForecast.append(night);
    dayForecast.append(wind);
    dayForecast.append(pressure);
    dayForecast.append(humidity);
    main.append(dayForecast);
}

function ConvertDate(seconds) {
    var date = new Date(0);
    date.setUTCSeconds(seconds);
    return date.toDateString();
}

window.onload = function () {
    GenerateForecast();
};