﻿$(document).ready(function () {
    AutoComplete("#cityName", "/Trip/GetCityListForTripAutocomplete", true, "/Trip/GetTripListFromSelectedcity");
    DatePicker();
    document.getElementById("datePickerFrom").querySelectorAll("input")[0].value = "";
    document.getElementById("datePickerTo").querySelectorAll("input")[0].value = "";
});

function AutoComplete(divId, urlAutocomplete, isCallback, urlSelected) {
    $(divId).autocomplete({
        source: function (request, response) {
            $.ajax({
                url: urlAutocomplete,
                type: "POST",
                dataType: "json",
                data: { Prefix: request.term },
                success: function (data) {
                    response($.map(data, function (item) {
                        return { label: item, value: item };
                    }));
                }
            });
        },
        select: function (event, ui) {
            event.preventDefault();
            $(divId).val(ui.item.label);
            if (isCallback === true) {
                $.ajax({
                    url: urlSelected,
                    type: "POST",
                    dataType: "json",
                    data: { Prefix: ui.item.label }
                });
            }
        },
        messages: {
            noResults: "", results: ""
        }
    });
}

function GetSelectedTrips() {
    var cityInput = document.getElementById("cityName");
    if (cityInput.value == "") {
        alert("Please enter city first!");
    }
    else {
        var placeForm = document.getElementById("placeForm");
        CleanInput(placeForm);
        var tripList = GetTripList("/Trip/GetTripList");
        CleanGallery("resultTrips");
        GenerateTripBox(tripList);
    }
}

function GetTripList(urlGetTripList) {
    var tripList;
    $.ajax({
        type: "POST",
        url: urlGetTripList,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            tripList = data;
        },
        async: false,
        failure: function (errMsg) {
            alert(errMsg);
        }
    });
    return tripList;
}

function GenerateTripBox(tripList) {
    for (var i = 0; i < tripList.length; i++) {
        var date = tripList[i].StartTime;
        date = parseInt(date.replace(/[^0-9\.]/g, ''), 10);
        var descriptionHtml = "Start time: " + new Date(date).toLocaleString() + "<br>Trip duration: " + new Date(tripList[i].DurationInSeconds * 1000).toLocaleTimeString() + "<br>Trip distance: " + tripList[i].DistanceInMeters + " m."
        var gallery = document.getElementById("resultTrips");
        var tripBox = CreateTripDiv("tripBox", "tripBox", tripList[i].Id, tripList[i].StartTime, "");
        gallery.appendChild(tripBox);
        var tripBoxPhoto = CreatePlaceDiv("tripBoxPhoto", "tripBoxPhoto", "", "");
        tripBox.appendChild(tripBoxPhoto);
        var tripBoxPrice = CreatePlaceDiv("tripBoxPrice", "tripBoxPrice", "", "");
        tripBox.appendChild(tripBoxPrice);
        var tripBoxPriceLabel = CreatePlaceDiv("tripBoxPriceLabel", "tripBoxPriceLabel", "", "Trip price:");
        tripBoxPrice.appendChild(tripBoxPriceLabel);
        var tripBoxPriceNumber = CreatePlaceDiv("tripBoxPriceNumber", "tripBoxPriceNumber", "", tripList[i].TripPrice + "$");
        tripBoxPrice.appendChild(tripBoxPriceNumber);
        var img = CreateImg(tripList[i].TripName, "placeBoxImage", tripList[i].Photo, tripList[i].TripName);
        tripBoxPhoto.appendChild(img);
        var tripBoxDescription = CreatePlaceDiv("tripBoxDescription", "tripBoxDescription", "", "");
        tripBox.appendChild(tripBoxDescription);
        var tripBoxName = CreatePlaceDiv("tripBoxName", "tripBoxName", "", tripList[i].TripName);
        tripBoxDescription.appendChild(tripBoxName);
        var tripBoxSummary = CreatePlaceDiv("tripBoxSummary", "tripBoxSummary", "tripBoxSummary", descriptionHtml);
        tripBoxDescription.appendChild(tripBoxSummary);
        var link = CreateA("tripBoxLink", "tripBoxLink", "/Trip/ShowTrip?id=" + tripList[i].Id, "Go to description!");
        tripBoxDescription.appendChild(link);
    }
}

function CleanGallery(galleryId) {
    var gallery = document.getElementById(galleryId);
    while (gallery.hasChildNodes()) {
        gallery.removeChild(gallery.firstChild);
    }
}

function CleanInput(input) {
    for (var i = 0; i < input.length; i++) {
        input[i].value = '';
    }
}

function DatePicker() {
    $('#datePickerFrom').datetimepicker({
        format: 'YYYY/MM/DD hh:mm'
    });
    $('#datePickerTo').datetimepicker({
        useCurrent: false,
        format: 'YYYY/MM/DD hh:mm'
    });
    $("#datePickerFrom").on("dp.change", function (e) {
        $('#datePickerTo').data("DateTimePicker").minDate(e.date);
    });
    $("#datePickerTo").on("dp.change", function (e) {
        $('#datePickerFrom').data("DateTimePicker").maxDate(e.date);
    });
}

function SortTripsByDate() {
    var tripList = document.getElementById("resultTrips").querySelectorAll(".tripBox");
    if (tripList == null | tripList.length < 1) {
        alert("There is no trips are choosen, please enter city!");
    }
    else {
        var dateFromInput = document.getElementById("datePickerFrom").querySelectorAll("input")[0].value;
        var dateToInput = document.getElementById("datePickerTo").querySelectorAll("input")[0].value;

        if (dateFromInput == '' | dateFromInput == null) {
            alert("Enter start date!");
        } else if (dateToInput == '' | dateToInput == null) {
            alert("Enter end date!");
        } else {

            var dateFromInMs = Date.parse(dateFromInput);
            console.log(dateFromInMs);
            var dateToInMs = Date.parse(dateToInput);
            console.log(dateToInMs);

            var tripsDiv = [];
            for (var i = 0; i < tripList.length; i++) {
                var id = tripList[i].getAttribute("data-id");
                var date = tripList[i].getAttribute("data-time");
                date = parseInt(date.replace(/[^0-9\.]/g, ''), 10);

                tripList[i].style.display = 'block';

                if (date <= dateFromInMs | date >= dateToInMs) {
                    tripList[i].style.display = 'none';
                }
                var trip = { Id: id, Date: date };
                tripsDiv.push(trip);
            };
        }
    }
}