﻿$(document).ready(function () {
    $("#googleAutocompletePlace").keyup(checkInput);
});

var googlePlace;

function checkInput() {
    if (this.value.length > 2) {
        cityAutocomplete();
    }
    else if (!this.value) {
        removeInput();
    }
};

function removeInput() {
    var cloneInput = $("#googleAutocompletePlace").clone();
    var oldInput = $('#googleAutocompletePlace').remove();
    cloneInput.prependTo('#googleAutocompletePlaceForm');
    $("#googleAutocompletePlace").keyup(checkInput).focus();
};

function cityAutocomplete() {
    var input = document.getElementById('googleAutocompletePlace');
    var autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.addListener('place_changed', function ShowDetails() {
        var place = autocomplete.getPlace();
        var isInDatabase = CheckPlace(place.place_id);
        if (isInDatabase) {
            alert("Place already in the database!");
            $("#placeAdditional").empty();
        } else {
            GetPlaceDetails(place.place_id);
        }
    });
};

function CheckPlace(placeId) {
    var isInDatabase;
    $.ajax({
        type: "POST",
        url: "/Trip/DownloadPlace?googleId=" + placeId,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            isInDatabase = data;
        },
        async: false,
        failure: function (errMsg) {
            alert(errMsg);
        }
    });
    return isInDatabase;
};

function GetPlaceDetails(placeId) {
    var map = new google.maps.Map(document.getElementById('map'));
    var request = {
        placeId: placeId
    };
    service = new google.maps.places.PlacesService(map);
    service.getDetails(request, ShowDetails);
};

function ShowDetails(place, status) {
    if (status == google.maps.places.PlacesServiceStatus.OK) {
        googlePlace = place;
        $("#placeAdditional").empty();
        CreateNewInputs(place);
    }
};

function CreateNewInputs(place) {
    var countryName = FindCountryStateCity(place, 'country', 'political');
    var input = CreateInputBlock('CountryName', countryName, 'Country name: ', 'block');
    $("#placeAdditional").append(input);
    var stateName = FindCountryStateCity(place, 'administrative_area_level_1', 'political');
    input = CreateInputBlock('StateName', stateName, 'State name: ', 'block');
    $("#placeAdditional").append(input);
    var cityName = FindCountryStateCity(place, 'locality', 'political');
    input = CreateInputBlock('CityName', cityName, 'City name: ', 'block');
    $("#placeAdditional").append(input);
    input = CreateInputBlock('PlaceName', place.name, 'Place name: ', 'block');
    $("#placeAdditional").append(input);
    input = CreateInputBlock('FormattedAddress', place.formatted_address, 'Address: ', 'block');
    $("#placeAdditional").append(input);
    if (place.international_phone_number != null) {
        input = CreateInputBlock('InternationalPhoneNumber', place.international_phone_number, 'Phone: ', 'block');
        $("#placeAdditional").append(input);
    }
    input = CreateInputBlock('GooglePlaceId', place.place_id, 'Place Id: ', 'none');
    $("#placeAdditional").append(input);
    if (place.rating != null) {
        input = CreateInputBlock('Rating', place.rating, 'Place rating: ', 'block');
        $("#placeAdditional").append(input);
    }
    input = CreateInputBlock('Reference', place.reference, 'Reference: ', 'none');
    $("#placeAdditional").append(input);
    input = CreateInputBlock('Types', GetTypes(place), 'Place types: ', 'block');
    $("#placeAdditional").append(input);
    input = CreateInputBlock('Latitude', place.geometry.location.lat(), 'Place latitude: ', 'block');
    $("#placeAdditional").append(input);
    input = CreateInputBlock('Longitude', place.geometry.location.lng(), 'Place longitude: ', 'block');
    $("#placeAdditional").append(input);
    input = CreateInputBlockDuration('Duration', '', 'Place duration: ', 'block');
    $("#placeAdditional").append(input);
    $("input[name='Duration']").datetimepicker({ format: 'HH:mm' });
    input = CreateDescriptionInputBlock('Description', '', 'Description: ', 'block', place.name);
    $("#placeAdditional").append(input);
    if (place.opening_hours != null) {
        input = CreateInputBlock('OpeningHours', GetOpeningHours(place), 'Opening hours: ', 'block');
        $("#placeAdditional").append(input);
    }
    if (place.website != null) {
        input = CreateInputBlock('Website', place.website, 'Place website: ', 'block');
        $("#placeAdditional").append(input);
    }
    input = CreateUploadPhotoBlock('Upload photo: ', 'block', place.name);
    $("#placeAdditional").append(input);
};

function FindCountryStateCity(place, firstOption, secondOption) {
    var address = place.address_components;
    for (var i = 0; i < address.length; i++) {
        var addressTypes = address[i].types;
        if (addressTypes.length == 2) {
            if ((addressTypes[0] == firstOption | addressTypes[0] == secondOption) & (addressTypes[1] == firstOption | addressTypes[1] == secondOption)) {
                return address[i].long_name;
            }
        }
    }
};

function GetTypes(place) {
    var types = '';
    for (var i = 0; i < place.types.length; i++) {
        types += place.types[i] + '; ';
    }
    return types;
};

function GetOpeningHours(place) {
    var openingHours = '';
    for (var i = 0; i < place.opening_hours.weekday_text.length; i++) {
        openingHours += place.opening_hours.weekday_text[i] + '; ';
    }
    return openingHours;
};

function CreateInputBlock(inputName, inputValue, labelText, display) {
    var inputDiv = $('<div>').attr({ class: "col-md-10" });
    var input = $('<input>').attr({ class: "form-control", name: inputName, required: "required", value: inputValue/*, readonly: "readonly"*/ });
    var labelError = $('<label>').attr({ class: "text-danger" });
    var formDiv = $('<div>').attr({ class: "form-group" }).css("display", display);
    var formLabel = $('<label>').attr({ class: "col-md-2 control-label" }).text(labelText);
    inputDiv.append(input);
    inputDiv.append(labelError);
    formDiv.append(formLabel);
    formDiv.append(inputDiv);
    return formDiv;
};

function CreateInputBlockDuration(inputName, inputValue, labelText, display) {
    var inputDiv = $('<div>').attr({ class: "col-md-10" });
    var input = $('<input>').attr({ class: "form-control", name: inputName, required: "required", value: inputValue });
    var labelError = $('<label>').attr({ class: "text-danger" });
    var formDiv = $('<div>').attr({ class: "form-group" }).css("display", display);
    var formLabel = $('<label>').attr({ class: "col-md-2 control-label" }).text(labelText);
    inputDiv.append(input);
    inputDiv.append(labelError);
    formDiv.append(formLabel);
    formDiv.append(inputDiv);
    return formDiv;
};

function CreateDescriptionInputBlock(inputName, inputValue, labelText, display, searchString) {
    var inputDiv = $('<div>').attr({ class: "col-md-10" });
    var input = $('<textarea>').attr({ class: "form-control", rows: "3", placeholder: "Place description", name: inputName, required: "required", value: inputValue });
    var labelError = $('<label>').attr({ class: "text-danger" });
    var formDiv = $('<div>').attr({ class: "form-group" }).css("display", display);
    var formLabel = $('<label>').attr({ class: "col-md-2 control-label" }).text(labelText);
    var descriptionLink = $("<a />", { target: "_blank", name: "descriptionLink", href: "https://www.google.com.ua/search?q=" + searchString, text: "You can find description using Google!" });
    inputDiv.append(input);
    inputDiv.append(labelError);
    inputDiv.append(descriptionLink);
    formDiv.append(formLabel);
    formDiv.append(inputDiv);
    return formDiv;
};

function CreateSelectImageUploadBlock(inputName, labelText, display) {
    var inputDiv = $('<div>').attr({ class: "col-md-10" });
    var select = $('<select>').attr({ class: "form-control", id: "selectPhotoUpload", name: inputName, onChange: "ChangeImageUpload()" });
    $('<option />', { value: '0', text: 'Upload your own image' }).appendTo(select);
    $('<option />', { value: '1', text: 'Select one of the existing images' }).appendTo(select);
    var labelError = $('<label>').attr({ class: "text-danger" });
    var formDiv = $('<div>').attr({ class: "form-group" }).css("display", display);
    var formLabel = $('<label>').attr({ class: "col-md-2 control-label" }).text(labelText);
    inputDiv.append(select);
    inputDiv.append(labelError);
    formDiv.append(formLabel);
    formDiv.append(inputDiv);
    return formDiv;
};

function CreateUploadPhotoBlock(labelText, display, searchString) {
    var inputDiv = $('<div>').attr({ class: "col-md-10" });
    var input = $('<input>').attr({ class: "btn btn-default", type: "file", id: "PlacePhoto", name: "PlacePhoto", value: "Select file" });
    var labelError = $('<label>').attr({ class: "text-danger" });
    var formDiv = $('<div>').attr({ class: "form-group", id: "UploadOwnPhoto" }).css("display", display);
    var formLabel = $('<label>').attr({ class: "col-md-2 control-label" }).text(labelText);
    var photoLink = $("<a />", { target: "_blank", name: "descriptionLink", href: "http://www.google.com/images?q=" + searchString, text: "You can find photo using Google!" });
    inputDiv.append(input);
    inputDiv.append(labelError);
    inputDiv.append(photoLink);
    formDiv.append(formLabel);
    formDiv.append(inputDiv);
    return formDiv;
};

function swapImage() {
    var url = document.getElementById("photoList").value;
    $('#image').attr('src', url);
};

//get file size
function GetFileSize(fileid) {
    try {
        var fileSize = 0;
        //for IE
        if ($.browser.msie) {
            var objFSO = new ActiveXObject("Scripting.FileSystemObject"); var filePath = $("#" + fileid)[0].value;
            var objFile = objFSO.getFile(filePath);
            var fileSize = objFile.size; //size in kb
            fileSize = fileSize / 1048576; //size in mb
        }
        //for FF, Safari, Opeara and Others
        else {
            fileSize = $("#" + fileid)[0].files[0].size //size in kb
            fileSize = fileSize / 1048576; //size in mb
        }
        return fileSize;
    }
    catch (e) {
        //alert("Error is :" + e);
    }
};

//get file path from client system
function getNameFromPath(strFilepath) {
    var objRE = new RegExp(/([^\/\\]+)$/);
    var strName = objRE.exec(strFilepath);
    if (strName == null) {
        return null;
    }
    else {
        return strName[0];
    }
};

$(function () {
    $("#PlacePhoto").change(function () {
        var file = getNameFromPath($(this).val());
        if (file != null) {
            var extension = file.substr((file.lastIndexOf('.') + 1));
            switch (extension) {
                case 'jpg':
                case 'png':
                case 'gif':
                    flag = true;
                    break;
                default:
                    flag = false;
            }
        }
        if (flag == false) {
            $("#PlacePhotoDiv > span").text("You can upload only jpg, png, gif extension file");
            return false;
        }
        else {
            var size = GetFileSize('PlacePhoto');
            if (size > 3) {
                $("#PlacePhotoDiv > span").text("You can upload file up to 3 MB");
            }
            else {
                $("#PlacePhotoDiv > span").text("");
            }
        }
    });
});