﻿$(document).ready(function () {
    AutoComplete("#cityName", "/Trip/GetCityListForAutocomplete", true, "/Trip/GetPlaceListFromSelectedcity");
    DragAndDrop();
});

function AutoComplete(divId, urlAutocomplete, isCallback, urlSelected) {
    $(divId).autocomplete({
        source: function (request, response) {
            $.ajax({
                url: urlAutocomplete,
                type: "POST",
                dataType: "json",
                data: { Prefix: request.term },
                success: function (data) {
                    response($.map(data, function (item) {
                        return { label: item.Name, value: item.Id };
                    }));
                }
            });
        },
        select: function (event, ui) {
            event.preventDefault();
            $(divId).val(ui.item.label);
            if (isCallback === true) {
                $.ajax({
                    url: urlSelected,
                    type: "POST",
                    dataType: "json",
                    data: { Prefix: ui.item.value }
                });
            }
        },
        messages: {
            noResults: "", results: ""
        }
    });
}

function GetSelectedPlaces() {
    var cityInput = document.getElementById("cityName");
    if (cityInput.value == '') {
        alert("Please enter city first!");
    }
    else {
        var placeForm = document.getElementById("placeForm");
        CleanInput(placeForm);
        var placeList = GetPlaceList("/Trip/GetPlaceList");
        CleanGallery("gallery-one");
        CleanGallery("gallery-two");
        GeneratePlaceBox(placeList);
    }
}

function GetPlaceList(urlGetPointList) {
    var pointList;
    $.ajax({
        type: "POST",
        url: urlGetPointList,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            pointList = data;
        },
        async: false,
        failure: function (errMsg) {
            alert(errMsg);
        }
    });
    return pointList;
}

function GeneratePlaceBox(placeList) {
    for (var i = 0; i < placeList.length; i++) {
        var gallery = document.getElementById("gallery-two");
        var placeBox = CreatePlaceDiv("placeBox", "placeBox", placeList[i].Id, "");
        gallery.appendChild(placeBox);
        var placeBoxPhoto = CreatePlaceDiv("placeBoxPhoto", "placeBoxPhoto", "", "");
        placeBox.appendChild(placeBoxPhoto);
        var img = CreateImg(placeList[i].Name, "placeBoxImage", placeList[i].Photo, placeList[i].Name);
        placeBoxPhoto.appendChild(img);
        var placeBoxDescription = CreatePlaceDiv("placeBoxDescription", "placeBoxDescription", "", placeList[i].Name);
        placeBox.appendChild(placeBoxDescription);
        var placeBoxLink = CreatePlaceDiv("placeBoxLink", "placeBoxLink", "", "");
        placeBox.appendChild(placeBoxLink);
        var link = CreateA("descriptionLink", "descriptionLink", "/Trip/ShowPlace?id=" + placeList[i].Id, "Go to description!");
        placeBoxLink.appendChild(link);
    };
}

function CleanGallery(galleryId) {
    var gallery = document.getElementById(galleryId);
    while (gallery.hasChildNodes()) {
        gallery.removeChild(gallery.firstChild);
    }
}

function CleanInput(input) {
    for (var i = 0; i < input.length; i++) {
        input[i].value = '';
    }
}

function ShowCreatedTrip() {
    var elements = document.getElementById("gallery-one").querySelectorAll(".placeBox");
    if (elements == null | elements.length < 1) {
        alert("There is no places are choosen, please enter city or drag places to your trip!");
    }
    else {
        var names = [];
        for (var i = 0; i < elements.length; i++) {
            names.push(elements[i].getAttribute("data-id"));
        };
        $.ajax({
            url: "/Trip/ShowCreatedTrip",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify(names),
            async: false
        });
        window.open("/Trip/CheckCreatedTrip", "_self");
    }
};