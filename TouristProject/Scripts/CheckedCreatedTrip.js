﻿function Initialize(number = 1) {
    var map = InitMap('map');
    var directionsService = new google.maps.DirectionsService;
    var directionsDisplay = new google.maps.DirectionsRenderer;
    directionsDisplay.setMap(map);
    var pointList = GetPointList("/Trip/GetCreatedTrip");
    var optimizedPointList = CalculateClosestPoint(pointList);
    var markerList = GetMarkerList(optimizedPointList);
    CalculateAndDisplayRoute(directionsService, directionsDisplay, markerList, "/Trip/GetGoogleJsonOnServer", optimizedPointList);
}

function CalculateAndDisplayRoute(directionsService, directionsDisplay, markerList, urlSendJsonRoutes, pointList) {
    var wayPoints = [];
    var html;
    for (var i = 0; i < markerList.length; i++) {
        if (i !== 0 & i !== markerList.length - 1) {
            wayPoints.push(
                {
                    location: markerList[i],
                    stopover: true
                });
        }
    }
    var request = {
        origin: markerList[0],
        destination: markerList[markerList.length - 1],
        waypoints: wayPoints,
        optimizeWaypoints: true,
        travelMode: google.maps.TravelMode.WALKING
    };
    directionsService.route(request,
        function GetResponse(response, status) {
            if (status === google.maps.DirectionsStatus.OK) {
                directionsDisplay.setDirections(response);
                SendRoutesToServer(response, urlSendJsonRoutes);
                var wayPointList = GetWayPointList(response);
                var optimizedPointList = GetOptimizedPointList(response, pointList);
                GenerateCreatedTrip(optimizedPointList, wayPointList);
            }
            else {
                window.alert('Directions request failed due to ' + status);
            }
        });
}

function CalculateDistantPoint(pointList) {
    var distance = 0;
    var lastPoint = pointList.length - 1;
    var optimizedPointList = pointList.slice();
    var pos1 = new google.maps.LatLng(pointList[0]["Latitude"], pointList[0]["Longitude"]);
    for (var i = 1; i < pointList.length; i++) {
        var pos2 = new google.maps.LatLng(pointList[i]["Latitude"], pointList[i]["Longitude"]);
        var newDistance = google.maps.geometry.spherical.computeDistanceBetween(pos1, pos2);
        if (newDistance > distance) {
            distance = newDistance;
            lastPoint = i;
        }
    }
    var tempPoint = optimizedPointList[optimizedPointList.length - 1];
    optimizedPointList[optimizedPointList.length - 1] = optimizedPointList[lastPoint];
    optimizedPointList[lastPoint] = tempPoint;
    return optimizedPointList;
}

function CalculateClosestPoint(pointList) {
    var distance = 20037519;
    var closePoint = pointList.length - 1;
    var optimizedPointList = pointList.slice();
    var pos1 = new google.maps.LatLng(pointList[0]["Latitude"], pointList[0]["Longitude"]);
    for (var i = 1; i < pointList.length; i++) {
        var pos2 = new google.maps.LatLng(pointList[i]["Latitude"], pointList[i]["Longitude"]);
        var newDistance = google.maps.geometry.spherical.computeDistanceBetween(pos1, pos2);
        if (newDistance < distance) {
            distance = newDistance;
            lastPoint = i;
        }
    }
    var tempPoint = optimizedPointList[optimizedPointList.length - 1];
    optimizedPointList[optimizedPointList.length - 1] = optimizedPointList[lastPoint];
    optimizedPointList[lastPoint] = tempPoint;
    return optimizedPointList;
}

function GetWayPointList(json) {
    var legs = json.routes[0].legs;
    return legs;
}

function GetOptimizedPointList(json, pointList) {
    var wayPointOrder = json.routes[0].waypoint_order;
    var optimizedPointList = [];
    optimizedPointList.push(pointList[0]);
    for (var i = 0; i < pointList.length - 2; i++) {
        var j = wayPointOrder[i];
        optimizedPointList[i + 1] = pointList[j + 1];
    }
    optimizedPointList.push(pointList[pointList.length - 1]);
    return optimizedPointList;
}

function GenerateCreatedTrip(optimizedPointList, wayPointList) {
    for (var i = 0; i < optimizedPointList.length; i++) {
        GeneratePlaceBox(optimizedPointList[i]);
        if (i < optimizedPointList.length - 1) {
            GenerateCourseBox(wayPointList[i]);
        }
    };
}

function GeneratePlaceBox(place) {
    var gallery = document.getElementById("gallery-one");
    var placeBox = CreatePlaceDiv("placeBox", "placeBox", place.Id, "");
    gallery.appendChild(placeBox);
    var placeBoxPhoto = CreatePlaceDiv("placeBoxPhoto", "placeBoxPhoto", "", "");
    placeBox.appendChild(placeBoxPhoto);
    var img = CreateImg(place.Name, "placeBoxImage", place.Photo, place.Name);
    placeBoxPhoto.appendChild(img);
    var placeBoxDescription = CreatePlaceDiv("placeBoxDescription", "placeBoxDescription", "", place.Description);
    placeBox.appendChild(placeBoxDescription);
    var placeBoxLink = CreatePlaceDiv("placeBoxLink", "placeBoxLink", "", "");
    placeBox.appendChild(placeBoxLink);
    var link = CreateA("descriptionLink", "descriptionLink", "/Trip/ShowPlace?id=" + place.Id, "Go to description!");
    placeBoxLink.appendChild(link);
}

function GenerateCourseBox(wayPoint) {
    var html = "Travel walking, approximate distance = " + wayPoint.distance.text + ", about = " + wayPoint.duration.text;
    var gallery = document.getElementById("gallery-one");
    var courseBox = CreateCourseDiv("courseBox", "courseBox", wayPoint.distance.value, wayPoint.duration.value, "");
    gallery.appendChild(courseBox);
    var courseBoxPhoto = CreateCourseDiv("courseBoxPhoto", "courseBoxPhoto", "", "", "");
    courseBox.appendChild(courseBoxPhoto);
    var img = CreateImg("img", "img", "/Content/Images/arrow.png", "arrow");
    courseBoxPhoto.appendChild(img);
    var courseBoxDescription = CreateCourseDiv("courseBoxDescription", "courseBoxDescription", "", "", html);
    courseBox.appendChild(courseBoxDescription);
}

function GetAddInfo(elements, dataAttribute) {
    var addInfo = '';
    for (var i = 0; i < elements.length; i++) {
        if (i === elements.length - 1) {
            addInfo += elements[i].getAttribute(dataAttribute);
        }
        else {
            addInfo += elements[i].getAttribute(dataAttribute) + ",";
        }
    };
    return addInfo;
}

function SubmitCreatedTrip() {
    var placeElements = document.getElementById("gallery-one").querySelectorAll(".placeBox");
    var ids = GetAddInfo(placeElements, "data-id");
    var courseElements = document.getElementById("gallery-one").querySelectorAll(".courseBox");
    var duration = GetAddInfo(courseElements, "data-duration");
    var distance = GetAddInfo(courseElements, "data-distance");
    var url = "/Trip/SubmitCreatedTrip?id=" + ids + "&duration=" + duration + "&distance=" + distance;
    $.ajax({
        url: url,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false
    });
    window.open("/Trip/SubmitCreatedTrip", "_self");
};