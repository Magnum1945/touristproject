﻿namespace LetsTravel.Entities.GoogleMapsModels
{
    public class GoogleAddress
    {
        public string Country { get; set; }
        public string State { get; set; }
        public string City { get; set; }
    }
}