﻿using System.Collections.Generic;

namespace LetsTravel.Entities.GoogleMapsModels
{
    public class GoogleCity
    {
        public class AddressComponent
        {
            public string long_name { get; set; }
            public string short_name { get; set; }
            public IList<string> types { get; set; }
        }

        public class Location
        {
            public double lat { get; set; }
            public double lng { get; set; }
        }

        public class Viewport
        {
            public double south { get; set; }
            public double west { get; set; }
            public double north { get; set; }
            public double east { get; set; }
        }

        public class Geometry
        {
            public Location location { get; set; }
            public Viewport viewport { get; set; }
        }

        public class Photo
        {
            public int height { get; set; }
            public IList<string> html_attributions { get; set; }
            public int width { get; set; }
        }

        public class Example
        {
            public IList<AddressComponent> address_components { get; set; }
            public string adr_address { get; set; }
            public string formatted_address { get; set; }
            public Geometry geometry { get; set; }
            public string icon { get; set; }
            public string id { get; set; }
            public string name { get; set; }
            public IList<Photo> photos { get; set; }
            public string place_id { get; set; }
            public string reference { get; set; }
            public string scope { get; set; }
            public IList<string> types { get; set; }
            public string url { get; set; }
            public int utc_offset { get; set; }
            public string vicinity { get; set; }
            public IList<object> html_attributions { get; set; }
        }
    }
}