﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace LetsTravel.Entities.ViewModels
{
    public class TripViewModel
    {
        [Required(ErrorMessage = "Please Enter Country Name")]
        [Display(Name = "Country name")]
        public string CountryName { get; set; }
        [Required(ErrorMessage = "Please Enter State Name")]
        [Display(Name = "State name")]
        public string StateName { get; set; }
        [Required(ErrorMessage = "Please Enter City Name")]
        [Display(Name = "City name")]
        public string CityName { get; set; }
        [Required(ErrorMessage = "Please Enter Trip Name")]
        [Display(Name = "Trip name")]
        public string TripName { get; set; }
        [Required(ErrorMessage = "Please Enter Owner Login")]
        [Display(Name = "User Login")]
        public string UserLogin { get; set; }
        [Required(ErrorMessage = "Please Enter Trip Description")]
        public string Description { get; set; }
        [Required(ErrorMessage = "Please Enter Place Duration")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd-MM-yyy HH:mm}")]
        public DateTime StartTime { get; set; }
        [Required(ErrorMessage = "Please Enter Place Duration")]
        [DataType(DataType.Time)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:H:mm}")]
        public TimeSpan Duration { get; set; }
        [Required(ErrorMessage = "Please Enter Trip Distance")]
        [Display(Name = "Trip distance")]
        public int Distance { get; set; }
        [Required(ErrorMessage = "Please Enter Trip Price Per Adult")]
        [Display(Name = "Trip price, $")]
        [RegularExpression(@"\d+(\.|,)?\d*", ErrorMessage = "Invalid price")]
        public string TripPrice { get; set; }
        [Required(ErrorMessage = "Please Upload File")]
        [Display(Name = "Upload File")]
        [ValidateFile]
        public HttpPostedFileBase TripPhoto { get; set; }
        public virtual List<Place> PlaceList { get; set; }
        public virtual List<User> UserList { get; set; }
        public virtual List<Post> PostList { get; set; }
    }
}