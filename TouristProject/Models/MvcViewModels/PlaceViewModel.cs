﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LetsTravel.Entities.ViewModels
{
    public class PlaceViewModel
    {
        [Required(ErrorMessage = "Please Enter Country Name")]
        [Display(Name = "Country name")]
        public string CountryName { get; set; }
        [Required(ErrorMessage = "Please Enter State Name")]
        [Display(Name = "State name")]
        public string StateName { get; set; }
        [Required(ErrorMessage = "Please Enter City Name")]
        [Display(Name = "City name")]
        public string CityName { get; set; }
        [Required(ErrorMessage = "Please Enter Place Name")]
        [Display(Name = "Place name")]
        public string PlaceName { get; set; }
        public string FormattedAddress { get; set; }
        public string InternationalPhoneNumber { get; set; }
        [Required]
        public string GooglePlaceId { get; set; }
        public string Rating { get; set; }
        public string Reference { get; set; }
        public string Types { get; set; }
        [Required(ErrorMessage = "Please Enter Place Description")]
        public string Description { get; set; }
        [Required(ErrorMessage = "Please Enter Place Latitude")]
        public string Latitude { get; set; }
        [Required(ErrorMessage = "Please Enter Place Longitude")]
        public string Longitude { get; set; }
        [Required(ErrorMessage = "Please Enter Place Duration")]
        [DataType(DataType.Time)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:H:mm}")]
        [Display(Name = "Duration")]
        public TimeSpan Duration { get; set; }
        [Display(Name = "Working hours")]
        public string OpeningHours { get; set; }
        public string Website { get; set; }
        [Required(ErrorMessage = "Please Upload File")]
        [Display(Name = "Upload File")]
        [ValidateFile]
        public HttpPostedFileBase PlacePhoto { get; set; }
    }

    public class ValidateFileAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            int MaxContentLength = 1024 * 1024 * 3;
            string[] AllowedFileExtensions = new string[] { ".jpg", ".gif", ".png" };

            var file = value as HttpPostedFileBase;

            if (file == null)
                return false;
            else if (!AllowedFileExtensions.Contains(file.FileName.Substring(file.FileName.LastIndexOf('.')).ToLower()))
            {
                ErrorMessage = "Please upload Your Photo of type: " + string.Join(", ", AllowedFileExtensions);
                return false;
            }
            else if (file.ContentLength > MaxContentLength)
            {
                ErrorMessage = "Your Photo is too large, maximum allowed size is : " + (MaxContentLength / 1024).ToString() + "MB";
                return false;
            }
            else
                return true;
        }
    }
}