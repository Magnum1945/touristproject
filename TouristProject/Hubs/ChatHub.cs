﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LetsTravel.DAL;
using LetsTravel.DAL.Abstractions;
using LetsTravel.Entities;
using Microsoft.AspNet.SignalR;

namespace LetsTravel.Hubs
{
    [Authorize]
    public class ChatHub : Hub
    {
        private UnitOfWork unitOfWork;
        private GenericRepository<Conversation> conversationRepository;
        public ChatHub()
        {
            this.unitOfWork = new UnitOfWork();
            this.conversationRepository = unitOfWork.GetRepository<Conversation>();
        }
        public ChatHub(ITouristModel touristModel)
        {
            this.unitOfWork = new UnitOfWork(touristModel);
            this.conversationRepository = unitOfWork.GetRepository<Conversation>();
        }

        public static Dictionary<string, string> connectedUsers = new Dictionary<string, string>();
        public void Send(string name, string message)
        {
            Clients.All.addNewMessageToPage(name, message);
        }
        public void SendToUser(string from, string to, string message)
        {
            Message newMessage = new Message()
            {
                Sender = from,
                MessageText = message,
                CreatedTime = DateTime.Now,
            };

            Conversation conversation = conversationRepository.Get(x => (x.User1 == from | x.User1 == to) & (x.User2 == from | x.User2 == to)).FirstOrDefault();
            if (conversation != null)
            {
                conversation.MessageList.Add(newMessage);
                conversationRepository.Update(conversation);
                unitOfWork.Save();
            }
            else
            {
                Conversation newConversation = new Conversation
                {
                    User1 = from,
                    User1_Id = connectedUsers.Where(x => x.Value == from).Select(x => x.Key).FirstOrDefault(),
                    User2 = to,
                    User2_Id = connectedUsers.Where(x => x.Value == to).Select(x => x.Key).FirstOrDefault(),
                    StartedTime = DateTime.Now,
                    MessageList = new List<Message>() { newMessage }
                };

                conversationRepository.Insert(newConversation);
                unitOfWork.Save();
            }
            string clientId = connectedUsers.Where(x => x.Value == to).Select(x => x.Key).FirstOrDefault();
            if (clientId != null)
            {
                Clients.Client(clientId).addNewMessageToPage(from, to, message);
            }
        }
        public void GetClientList()
        {
            Clients.All.addOnlineUsers(connectedUsers.Select(x => x.Value).ToArray());
        }
        public override Task OnConnected()
        {
            string id = Context.ConnectionId;
            string name = Context.User.Identity.Name;
            connectedUsers.Add(id, name);
            return base.OnConnected();
        }
        public override Task OnDisconnected(bool stopCalled)
        {
            string id = Context.ConnectionId;
            string name = Context.User.Identity.Name;
            connectedUsers.Remove(id);
            return base.OnDisconnected(stopCalled);
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}