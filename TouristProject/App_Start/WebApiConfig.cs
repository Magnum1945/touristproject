﻿using System.Web.Http;
using System.Web.Http.OData.Builder;
using System.Web.Http.OData.Extensions;
using Microsoft.Owin.Security.OAuth;
using LetsTravel.Entities;

namespace LetsTravel
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            // Configure Web API to use only bearer token authentication.
            config.SuppressDefaultHostAuthentication();
            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
            builder.EntitySet<User>("User");
            builder.EntitySet<Address>("Address");
            builder.EntitySet<Language>("Language");
            builder.EntitySet<Trip>("Trip");
            builder.EntitySet<Place>("Place");
            builder.EntitySet<City>("City");
            builder.EntitySet<State>("State");
            builder.EntitySet<Country>("Country");
            builder.EntitySet<Post>("Post");
            builder.EntitySet<Conversation>("Conversations");
            builder.EntitySet<Message>("Message");
            config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
        }
    }
}
