﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LetsTravel.Controllers.MvcApp
{
    [Authorize]
    public class DocsController : Controller
    {
        public ActionResult Main()
        {
            return View();
        }
    }
}