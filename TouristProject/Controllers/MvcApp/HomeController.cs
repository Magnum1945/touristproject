﻿using System.Web.Mvc;
using LetsTravel.DAL;

namespace LetsTravel.Controllers.MvcApp
{
    [RequireHttps]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult About()
        {
            ViewBag.Message = "Lets travel project!";

            return View();
        }
        public ActionResult Contact()
        {
            ViewBag.Message = "Contact us.";

            return View();
        }
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }
    }
}