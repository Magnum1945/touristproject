﻿using System.Web.Mvc;

namespace LetsTravel.Controllers.MvcApp
{
    public class UserController : Controller
    {
        // GET: User
        public ActionResult Registration()
        {
            return View();
        }
        public ActionResult Login()
        {
            return View();
        }
        public ActionResult Error()
        {
            return View();
        }
        public ActionResult ConfirmEmail()
        {
            return View();
        }
    }
}