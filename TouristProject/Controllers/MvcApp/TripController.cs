﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using LetsTravel.DAL;
using LetsTravel.DAL.Abstractions;
using LetsTravel.Entities;
using LetsTravel.Entities.OpenWeatherModels;
using LetsTravel.Entities.ViewModels;
using LetsTravel.Helpers;
using Newtonsoft.Json;

namespace LetsTravel.Controllers.MvcApp
{
    [Authorize]
    public class TripController : Controller
    {
        private IOpenWeather weatherHelper;
        private UnitOfWork unitOfWork;
        private GoogleHelper googleHelper;
        private GenericRepository<Place> placeRepository;
        private GenericRepository<Trip> tripRepository;
        private GenericRepository<City> cityRepository;
        private GenericRepository<State> stateRepository;
        private GenericRepository<Country> countryRepository;
        private GenericRepository<Conversation> conversationRepository;

        public TripController()
        {
            this.unitOfWork = new UnitOfWork();
            this.googleHelper = new GoogleHelper();
            this.placeRepository = unitOfWork.GetRepository<Place>();
            this.tripRepository = unitOfWork.GetRepository<Trip>();
            this.cityRepository = unitOfWork.GetRepository<City>();
            this.stateRepository = unitOfWork.GetRepository<State>();
            this.countryRepository = unitOfWork.GetRepository<Country>();
            this.conversationRepository = unitOfWork.GetRepository<Conversation>();
            this.weatherHelper = new OpenWeatherHelper();
        }

        public TripController(ITouristModel touristModel, IOpenWeather weatherHelper)
        {
            this.unitOfWork = new UnitOfWork(touristModel);
            this.googleHelper = new GoogleHelper();
            this.placeRepository = unitOfWork.GetRepository<Place>();
            this.tripRepository = unitOfWork.GetRepository<Trip>();
            this.cityRepository = unitOfWork.GetRepository<City>();
            this.stateRepository = unitOfWork.GetRepository<State>();
            this.countryRepository = unitOfWork.GetRepository<Country>();
            this.conversationRepository = unitOfWork.GetRepository<Conversation>();
            this.weatherHelper = weatherHelper;
        }

        [AllowAnonymous]
        public ActionResult StartView()
        {
            return View();
        }

        public ActionResult CreateTrip()
        {
            return View();
        }

        public ActionResult CheckCreatedTrip()
        {
            return View();
        }

        public ActionResult WeatherForecast()
        {
            return View();
        }

        public ActionResult SubmitCreatedTrip()
        {
            TripViewModel createdTrip = Session["CreatedTrip"] as TripViewModel;
            return View(createdTrip);
        }

        public ActionResult SelectTrip()
        {
            return View();
        }
        public ActionResult ShowPlace(int id)
        {
            Place place = placeRepository.GetByID(id);
            return View(place);
        }

        public ActionResult CreatePlace()
        {
            return View();
        }

        public ActionResult ShowTrip(int id)
        {
            Trip selectedTrip = tripRepository.Get(x => x.Id == id, includeProperties: "PlaceList").SingleOrDefault();
            Session["SelectedTrip"] = selectedTrip;
            return View(selectedTrip);
        }

        [HttpPost]
        public void SubmitCreatedTrip(string id, string duration, string distance)
        {
            string[] ids = id.Split(',');
            string[] durations = duration.Split(',');
            string[] distances = distance.Split(',');

            List<Place> selectedPlaceList = new List<Place>();
            double tripDuration = durations.Select(double.Parse).Sum();
            int tripDistance = distances.Select(int.Parse).Sum();

            foreach (string name in ids)
            {
                var place = placeRepository.GetByID(Int32.Parse(name));
                tripDuration += place.DurationInSeconds;
                selectedPlaceList.Add(place);
            }

            City city = cityRepository.GetByID(selectedPlaceList.First().CityId);
            State state = stateRepository.GetByID(city.StateId);
            Country country = countryRepository.GetByID(state.CountryId);

            string user = User.Identity.Name;
            TripViewModel createdTrip = new TripViewModel
            {
                CountryName = country.Name,
                StateName = state.Name,
                CityName = city.Name,
                Duration = TimeSpan.FromSeconds(tripDuration),
                Distance = tripDistance,
                PlaceList = selectedPlaceList,
                UserLogin = user,
            };

            Session["CreatedTrip"] = createdTrip;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveCreatedTrip(TripViewModel trip)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string filePath = string.Empty;
                    if (trip.TripPhoto != null && trip.TripPhoto.ContentLength > 0)
                    {
                        string fileName = System.IO.Path.GetFileName(trip.TripPhoto.FileName.ToLower());
                        string fileExtension = System.IO.Path.GetExtension(fileName);
                        Guid photoId = Guid.NewGuid();
                        filePath = $"/Content/Images/{trip.CountryName.ToLower()}/{trip.StateName.ToLower()}/{trip.CityName.ToLower()}/{photoId}{fileExtension}";
                        string path = Server.MapPath(filePath);
                        FileInfo fi = new FileInfo(path);
                        fi.Directory.Create();
                        trip.TripPhoto.SaveAs(path);
                    }

                    List<Place> newPlaceList = new List<Place>();
                    foreach (var place in trip.PlaceList)
                    {
                        newPlaceList.Add(placeRepository.GetByID(place.Id));
                    }

                    Mapper.Initialize(x => x.CreateMap<TripViewModel, Trip>()
                        .ForMember("DistanceInMeters", opt => opt.MapFrom(c => c.Distance))
                        .ForMember("DurationInSeconds", opt => opt.MapFrom(c => trip.Duration.TotalSeconds))
                        .ForMember("TripPrice", opt => opt.MapFrom(src => Convert.ToDecimal(trip.TripPrice, CultureInfo.InvariantCulture)))
                        .ForMember("Photo", opt => opt.MapFrom(src => filePath))
                        .ForMember("PlaceList", opt => opt.MapFrom(src => newPlaceList)));

                    Trip newTrip = Mapper.Map<TripViewModel, Trip>(trip);

                    int tripId = tripRepository.Get(x => x.TripName.ToLower() == trip.TripName.ToLower()).Select(x => x.Id).SingleOrDefault();
                    if (tripId == 0)
                    {
                        tripRepository.Insert(newTrip);
                        unitOfWork.Save();
                    }

                    return RedirectToAction("StartView");
                }
                else
                {
                    return View("SubmitCreatedTrip", trip);
                }
            }
            catch (RetryLimitExceededException /* dex */)
            {
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
            }

            return View("CreateTrip");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreatePlace(PlaceViewModel country)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string filePath = string.Empty;
                    if (country.PlacePhoto != null && country.PlacePhoto.ContentLength > 0)
                    {
                        string fileName = System.IO.Path.GetFileName(country.PlacePhoto.FileName.ToLower());
                        string fileExtension = System.IO.Path.GetExtension(fileName);
                        filePath = $"/Content/Images/{country.CountryName.ToLower()}/{country.StateName.ToLower()}/{country.CityName.ToLower()}/{country.GooglePlaceId}{fileExtension}";
                        string path = Server.MapPath(filePath);
                        FileInfo fi = new FileInfo(path);
                        fi.Directory.Create();
                        country.PlacePhoto.SaveAs(path);
                    }

                    double latitude = default(double);
                    if (country.Latitude != null)
                    {
                        latitude = double.Parse(country.Latitude, CultureInfo.InvariantCulture);
                    }

                    double longitude = default(double);
                    if (country.Longitude != null)
                    {
                        longitude = double.Parse(country.Longitude, CultureInfo.InvariantCulture);
                    }

                    double rating = default(double);
                    if (country.Rating != null)
                    {
                        rating = double.Parse(country.Rating, CultureInfo.InvariantCulture);
                    }

                    Mapper.Initialize(x => x.CreateMap<PlaceViewModel, Place>()
                        .ForMember("Name", opt => opt.MapFrom(c => country.PlaceName))
                        .ForMember("Latitude", opt => opt.MapFrom(c => latitude))
                        .ForMember("Longitude", opt => opt.MapFrom(c => longitude))
                        .ForMember("DurationInSeconds", opt => opt.MapFrom(c => country.Duration.TotalSeconds))
                        .ForMember("Photo", opt => opt.MapFrom(c => filePath))
                        .ForMember("Rating", opt => opt.MapFrom(c => rating))
                        .ForMember("DurationInSeconds", opt => opt.MapFrom(c => country.Duration.TotalSeconds)));

                    Place newPlace = Mapper.Map<PlaceViewModel, Place>(country);

                    City newCity = new City { Name = country.CityName, PlaceList = new List<Place> { newPlace } };
                    State newState = new State { Name = country.StateName, CityList = new List<City> { newCity } };
                    Country newCountry = new Country { Name = country.CountryName, StateList = new List<State> { newState } };

                    int countryId = countryRepository.Get(x => x.Name.ToLower() == country.CountryName.ToLower()).Select(x => x.Id).SingleOrDefault();
                    if (countryId == 0)
                    {
                        countryRepository.Insert(newCountry);
                    }
                    else
                    {
                        int stateId = stateRepository.Get(x => x.Name.ToLower() == country.StateName.ToLower()).Select(x => x.Id).SingleOrDefault();
                        if (stateId == 0)
                        {
                            newState.CountryId = countryId;
                            stateRepository.Insert(newState);
                        }
                        else
                        {
                            int cityId = cityRepository.Get(x => x.Name.ToLower() == country.CityName.ToLower()).Select(x => x.Id).SingleOrDefault();
                            if (cityId == 0)
                            {
                                newCity.StateId = stateId;
                                cityRepository.Insert(newCity);
                            }
                            else
                            {
                                int placeId = placeRepository.Get(x => x.Name.ToLower() == country.PlaceName.ToLower()).Select(x => x.Id).SingleOrDefault();
                                if (placeId == 0)
                                {
                                    newPlace.CityId = cityId;
                                    placeRepository.Insert(newPlace);
                                }
                            }
                        }
                    }

                    unitOfWork.Save();
                    return RedirectToAction("CreatePlace");
                }
            }
            catch (RetryLimitExceededException /* dex */)
            {
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
            }

            return View("CreatePlace");
        }

        [HttpPost]
        public JsonResult DownloadPlace(string googleId)
        {
            bool placeInDatabase = false;
            Place databasePlace = placeRepository.Get(x => x.GooglePlaceId == googleId).FirstOrDefault();
            if (databasePlace != null)
            {
                placeInDatabase = true;
            }

            return Json(placeInDatabase, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetCityListForAutocomplete(string Prefix)
        {
            List<KeyValuePair<int, string>> addressList = new List<KeyValuePair<int, string>>();
            var databaseCityList = cityRepository.Get(x => x.Name.ToLower().StartsWith(Prefix.ToLower()));
            foreach (City city in databaseCityList)
            {
                State state = stateRepository.GetByID(city.StateId);
                Country country = countryRepository.GetByID(state.CountryId);
                addressList.Add(new KeyValuePair<int, string>(city.Id, $"{city.Name}, {state.Name}, {country.Name}"));
            }

            return Json(addressList.Select(x => new { Id = x.Key, Name = x.Value }), JsonRequestBehavior.AllowGet);
        }

        public void GetPlaceListFromSelectedCity(int Prefix)
        {
            City city = cityRepository.GetByID(Prefix);
            Session["SelectedPlaceList"] = city.PlaceList;
        }

        [HttpPost]
        public JsonResult GetPlaceList()
        {
            List<Place> placeList = Session["SelectedPlaceList"] as List<Place>;
            Mapper.Initialize(x => x.CreateMap<Place, Place>()
                    .ForMember(y => y.City, opt => opt.Ignore())
                    .ForMember(y => y.TripList, opt => opt.Ignore()));
            List<Place> newPlaceList = new List<Place>();
            foreach (var place in placeList)
            {
                newPlaceList.Add(Mapper.Map<Place, Place>(place));
            }

            return Json(newPlaceList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public void ShowCreatedTrip(string[] names)
        {
            List<Place> placeList = Session["SelectedPlaceList"] as List<Place>;
            List<Place> selectedPlaceList = new List<Place>();
            foreach (string name in names)
            {
                selectedPlaceList.Add(placeList.Where(x => x.Id == Int32.Parse(name)).SingleOrDefault());
            }

            Session["SelectedPlaceList"] = selectedPlaceList;
        }

        [HttpPost]
        public JsonResult GetCreatedTrip()
        {
            List<Place> placeList = Session["SelectedPlaceList"] as List<Place>;
            Mapper.Initialize(x => x.CreateMap<Place, Place>()
                .ForMember(y => y.City, opt => opt.Ignore())
                .ForMember(y => y.TripList, opt => opt.Ignore()));
            List<Place> newPlaceList = new List<Place>();
            foreach (var place in placeList)
            {
                newPlaceList.Add(Mapper.Map<Place, Place>(place));
            }

            return Json(newPlaceList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public void GetGoogleJsonOnServer()
        {
            string jsonString = ReadStream();
            RootObject json = JsonConvert.DeserializeObject<RootObject>(jsonString);
            Session["GoogleJson"] = json;
        }

        [HttpPost]
        public JsonResult GetPlace(int id)
        {
            Place place = placeRepository.GetByID(id);
            Mapper.Initialize(x => x.CreateMap<Place, Place>()
                .ForMember(y => y.City, opt => opt.Ignore())
                .ForMember(y => y.TripList, opt => opt.Ignore()));
            Place newPlace = Mapper.Map<Place, Place>(place);

            return Json(newPlace, JsonRequestBehavior.AllowGet);
        }

        public string ReadStream()
        {
            var resolveRequest = HttpContext.Request;
            resolveRequest.InputStream.Seek(0, SeekOrigin.Begin);
            return new StreamReader(resolveRequest.InputStream).ReadToEnd();
        }

        [HttpPost]
        public JsonResult GetCityListForTripAutocomplete(string Prefix)
        {
            List<string> addressList = new List<string>();
            var databaseTripList = tripRepository.Get(x => x.CityName.ToLower().StartsWith(Prefix.ToLower()));
            foreach (var trip in databaseTripList)
            {
                addressList.Add($"{trip.CityName}, {trip.StateName}, {trip.CountryName}");
            }

            return Json(addressList.Distinct(), JsonRequestBehavior.AllowGet);
        }

        public void GetTripListFromSelectedCity(string Prefix)
        {
            string[] cityStateCountry = Prefix.Split(',');
            string city = cityStateCountry[0].Trim();
            string state = cityStateCountry[1].Trim();
            string country = cityStateCountry[2].Trim();
            var tripList = tripRepository.Get(
                x => x.CityName.ToLower() == city.ToLower()
                   & x.StateName.ToLower() == state.ToLower()
                   & x.CountryName.ToLower() == country.ToLower(), includeProperties: "PlaceList");
            var selectedTripList = tripList.ToList();

            Session["SelectedTripList"] = selectedTripList;
        }

        [HttpPost]
        public JsonResult GetTripList()
        {
            List<Trip> tripList = Session["SelectedTripList"] as List<Trip>;
            Mapper.Initialize(x => x.CreateMap<Trip, Trip>()
                .ForMember(y => y.UserList, opt => opt.Ignore())
                .ForMember(y => y.PostList, opt => opt.Ignore())
                .ForMember(y => y.PlaceList, opt => opt.Ignore()));
            List<Trip> newTripList = new List<Trip>();
            foreach (var trip in tripList)
            {
                newTripList.Add(Mapper.Map<Trip, Trip>(trip));
            }

            return Json(newTripList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetSelectedTrip()
        {
            Trip selectedTrip = Session["SelectedTrip"] as Trip;
            Mapper.Initialize(x => x.CreateMap<Place, Place>()
                .ForMember(y => y.City, opt => opt.Ignore())
                .ForMember(y => y.TripList, opt => opt.Ignore()));
            List<Place> newPlaceList = new List<Place>();
            foreach (var place in selectedTrip.PlaceList)
            {
                newPlaceList.Add(Mapper.Map<Place, Place>(place));
            }

            Mapper.Initialize(x => x.CreateMap<Trip, Trip>()
                .ForMember(y => y.UserList, opt => opt.Ignore())
                .ForMember(y => y.PostList, opt => opt.Ignore())
                .ForMember("PlaceList", opt => opt.MapFrom(src => newPlaceList)));
            Trip newTrip = Mapper.Map<Trip, Trip>(selectedTrip);

            return Json(newTrip, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetSelectedConversation(string from, string to)
        {
            Conversation conversation = conversationRepository.Get(x => (x.User1 == from | x.User1 == to) & (x.User2 == from | x.User2 == to), includeProperties: "MessageList").FirstOrDefault();
            if (conversation != null)
            {
                Mapper.Initialize(x => x.CreateMap<Message, Message>()
                    .ForMember(y => y.Conversation, opt => opt.Ignore()));
                List<Message> newMessageList = new List<Message>();
                foreach (var message in conversation.MessageList)
                {
                    newMessageList.Add(Mapper.Map<Message, Message>(message));
                }

                Mapper.Initialize(x => x.CreateMap<Conversation, Conversation>()
                    .ForMember("MessageList", opt => opt.MapFrom(src => newMessageList)));
                Conversation newConversation = Mapper.Map<Conversation, Conversation>(conversation);

                return Json(newConversation, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new Conversation(), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GetForecast(string city)
        {
            string result = weatherHelper.GetForecastData(city);
            OpenForecast.Example forecast = weatherHelper.ParseForecastData(result);
            return Json(forecast, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            unitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}