﻿using System.Data;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.OData;
using LetsTravel.DAL;
using LetsTravel.DAL.Abstractions;
using LetsTravel.Entities;
using LetsTravel.Helpers;

namespace LetsTravel.Controllers.WebApi
{
    [Authorize]
    public class PlaceController : ODataController
    {
        private UnitOfWork unitOfWork;
        private GenericRepository<Place> placeRepository;
        private GoogleHelper googleHelper;
        public PlaceController()
        {
            this.unitOfWork = new UnitOfWork();
            this.googleHelper = new GoogleHelper();
            this.placeRepository = unitOfWork.GetRepository<Place>();
        }
        public PlaceController(ITouristModel touristModel)
        {
            this.unitOfWork = new UnitOfWork(touristModel);
            this.googleHelper = new GoogleHelper();
            this.placeRepository = unitOfWork.GetRepository<Place>();
        }

        // GET: odata/Place
        [EnableQuery]
        public IQueryable<Place> GetPlace()
        {
            return placeRepository.Get();
        }

        //// GET: odata/Place(5)
        //[EnableQuery]
        //public SingleResult<Place> GetPlace([FromODataUri] int key)
        //{
        //    Place place = placeRepository.GetByID(key);
        //    return SingleResult.Create(new[] { place }.AsQueryable());
        //}

        // GET: odata/Place?key=googlePlaceId
        [EnableQuery]
        public SingleResult<Place> GetPlace(string key)
        {
            Place place = new Place(); 
            place = placeRepository.Get(x=>x.GooglePlaceId == key).FirstOrDefault();
            if (place == null)
            {
                place = googleHelper.DownloadPlaceDetails(key);
            }
            return SingleResult.Create(new[] { place }.AsQueryable());
        }

        // PUT: odata/Place(5)
        public IHttpActionResult Put([FromODataUri] int key, Delta<Place> patch)
        {
            Validate(patch.GetEntity());
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            Place place = placeRepository.GetByID(key);
            if (place == null)
            {
                return NotFound();
            }
            Place updatedPlace = patch.GetEntity();
            updatedPlace.Id = key;
            placeRepository.Update(place, updatedPlace);
            try
            {
                unitOfWork.Save();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PlaceExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: odata/Place
        public IHttpActionResult Post(Place place)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            Place databasePlace = placeRepository.Get(x => x.GooglePlaceId == place.GooglePlaceId).FirstOrDefault();
            if (databasePlace != null)
            {
                return BadRequest("Place already in the database");
            }
            placeRepository.Insert(place);
            unitOfWork.Save();
            return StatusCode(HttpStatusCode.NoContent);
        }

        // PATCH: odata/Place(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<Place> patch)
        {
            Validate(patch.GetEntity());
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            Place place = placeRepository.GetByID(key);
            if (place == null)
            {
                return NotFound();
            }
            Place updatedPlace = patch.GetEntity();
            updatedPlace.Id = key;
            placeRepository.Update(place, updatedPlace);
            try
            {
                unitOfWork.Save();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PlaceExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return StatusCode(HttpStatusCode.NoContent);
        }

        // DELETE: odata/Place(5)
        public IHttpActionResult Delete([FromODataUri] int key)
        {
            Place place = placeRepository.GetByID(key);
            if (place == null)
            {
                return NotFound();
            }
            placeRepository.Delete(place);
            unitOfWork.Save();
            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET: odata/Place(5)/City
        [EnableQuery]
        public SingleResult<City> GetCity([FromODataUri] int key)
        {
            var place = placeRepository.Get(x => x.Id == key, includeProperties: "City");
            return SingleResult.Create(place.Select(x => x.City).AsQueryable());
        }

        // GET: odata/Place(5)/TripList
        [EnableQuery]
        public IQueryable<Trip> GetTripList([FromODataUri] int key)
        {
            var place = placeRepository.Get(x => x.Id == key, includeProperties: "TripList");
            return place.SelectMany(x => x.TripList);
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }
        private bool PlaceExists(int key)
        {
            Place place = placeRepository.GetByID(key);
            return (place != null) ? true : false;
        }
    }
}
