﻿using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.OData;
using LetsTravel.DAL;
using LetsTravel.DAL.Abstractions;
using LetsTravel.Entities;

namespace LetsTravel.Controllers.WebApi
{
    [Authorize]
    public class CountryController : ODataController
    {
        private UnitOfWork unitOfWork;
        private GenericRepository<Country> countryRepository;
        private GenericRepository<State> stateRepository;
        private GenericRepository<City> cityRepository;
        private GenericRepository<Place> placeRepository;
        public CountryController()
        {
            this.unitOfWork = new UnitOfWork();
            this.countryRepository = unitOfWork.GetRepository<Country>();
            this.stateRepository = unitOfWork.GetRepository<State>();
            this.cityRepository = unitOfWork.GetRepository<City>();
            this.placeRepository = unitOfWork.GetRepository<Place>();
        }
        public CountryController(ITouristModel touristModel)
        {
            this.unitOfWork = new UnitOfWork(touristModel);
            this.countryRepository = unitOfWork.GetRepository<Country>();
            this.stateRepository = unitOfWork.GetRepository<State>();
            this.cityRepository = unitOfWork.GetRepository<City>();
            this.placeRepository = unitOfWork.GetRepository<Place>();
        }

        // GET: odata/Country
        [EnableQuery]
        public IQueryable<Country> GetCountry()
        {
            return countryRepository.Get();
        }

        // GET: odata/Country(5)
        [EnableQuery]
        public SingleResult<Country> GetCountry([FromODataUri] int key)
        {
            Country country = countryRepository.GetByID(key);
            return SingleResult.Create(new[] { country }.AsQueryable());
        }

        // PUT: odata/Country(5)
        public IHttpActionResult Put([FromODataUri] int key, Delta<Country> patch)
        {
            Validate(patch.GetEntity());
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            Country country = countryRepository.GetByID(key);
            if (country == null)
            {
                return NotFound();
            }
            Country updatedCountry = patch.GetEntity();
            updatedCountry.Id = key;
            countryRepository.Update(country, updatedCountry);
            try
            {
                unitOfWork.Save();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CountryExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: odata/Country
        public IHttpActionResult Post(Country country)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            int countryId = countryRepository.Get(x => x.Name.ToLower() == country.Name.ToLower()).Select(x => x.Id).SingleOrDefault();
            if (countryId == 0)
            {
                countryRepository.Insert(country);
            }
            else
            {
                if (country.StateList != null && country.StateList.Count != 0)
                {
                    State state = country.StateList.FirstOrDefault();
                    int stateId = stateRepository.Get(x => x.Name.ToLower() == state.Name.ToLower()).Select(x => x.Id).SingleOrDefault();
                    if (stateId == 0)
                    {
                        state.CountryId = countryId;
                        stateRepository.Insert(state);
                    }
                    else
                    {
                        if (state.CityList != null && state.CityList.Count != 0)
                        {
                            City city = state.CityList.FirstOrDefault();
                            int cityId = cityRepository.Get(x => x.Name.ToLower() == city.Name.ToLower()).Select(x => x.Id).SingleOrDefault();
                            if (cityId == 0)
                            {
                                city.StateId = stateId;
                                cityRepository.Insert(city);
                            }
                            else
                            {
                                if (city.PlaceList != null && city.PlaceList.Count != 0)
                                {
                                    Place place = city.PlaceList.FirstOrDefault();
                                    int placeId = placeRepository.Get(x => x.GooglePlaceId == place.GooglePlaceId).Select(x => x.Id).SingleOrDefault();
                                    if (placeId == 0)
                                    {
                                        place.CityId = cityId;
                                        placeRepository.Insert(place);
                                    }
                                    else
                                    {
                                        return BadRequest("Place already in the database");
                                    }
                                }
                                else
                                {
                                    return BadRequest("Place is missing");
                                }
                            }
                        }
                        else
                        {
                            return BadRequest("City is missing");
                        }
                    }
                }
                else
                {
                    return BadRequest("State is missing");
                }
            }

            unitOfWork.Save();
            return StatusCode(HttpStatusCode.NoContent);
        }

        // PATCH: odata/Country(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<Country> patch)
        {
            Validate(patch.GetEntity());
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            Country country = countryRepository.GetByID(key);
            if (country == null)
            {
                return NotFound();
            }
            Country updatedCountry = patch.GetEntity();
            updatedCountry.Id = key;
            countryRepository.Update(country, updatedCountry);
            try
            {
                unitOfWork.Save();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CountryExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return StatusCode(HttpStatusCode.NoContent);
        }

        // DELETE: odata/Country(5)
        public IHttpActionResult Delete([FromODataUri] int key)
        {
            Country country = countryRepository.GetByID(key);
            if (country == null)
            {
                return NotFound();
            }
            countryRepository.Delete(country);
            unitOfWork.Save();
            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET: odata/Country(5)/StateList
        [EnableQuery]
        public IQueryable<State> GetStateList([FromODataUri] int key)
        {
            var country = countryRepository.Get(x => x.Id == key, includeProperties: "StateList");
            return country.SelectMany(x => x.StateList);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CountryExists(int key)
        {
            Country country = countryRepository.GetByID(key);
            return (country != null) ? true : false;
        }
    }
}
