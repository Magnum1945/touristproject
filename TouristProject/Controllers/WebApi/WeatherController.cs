﻿using System.Web.Http;
using LetsTravel.Entities.OpenWeatherModels;
using LetsTravel.Helpers;

namespace LetsTravel.Controllers.WebApi
{
    [Authorize]
    public class WeatherController : ApiController
    {
        private IOpenWeather weatherHelper;
        public WeatherController()
        {
            weatherHelper = new OpenWeatherHelper();
        }
        public WeatherController(IOpenWeather weatherHelper)
        {
            this.weatherHelper = weatherHelper;
        }
        // GET: api/Weather?city=Lviv
        public OpenForecast.Example Get(string city)
        {
            OpenWeatherHelper weatherHelper = new OpenWeatherHelper();
            string result = weatherHelper.GetForecastData(city);
            OpenForecast.Example forecast = weatherHelper.ParseForecastData(result);
            return forecast;
        }
    }
}
