﻿using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.OData;
using LetsTravel.DAL;
using LetsTravel.DAL.Abstractions;
using LetsTravel.Entities;

namespace LetsTravel.Controllers.WebApi
{
    [Authorize]
    public class ConversationController : ODataController
    {
        private UnitOfWork unitOfWork;
        private GenericRepository<Conversation> conversationRepository;
        public ConversationController()
        {
            this.unitOfWork = new UnitOfWork();
            this.conversationRepository = unitOfWork.GetRepository<Conversation>();
        }
        public ConversationController(ITouristModel touristModel)
        {
            this.unitOfWork = new UnitOfWork(touristModel);
            this.conversationRepository = unitOfWork.GetRepository<Conversation>();
        }
        // GET: odata/Conversation
        [EnableQuery]
        public IQueryable<Conversation> GetConversation()
        {
            return conversationRepository.Get();
        }

        // GET: odata/Conversation(5)
        [EnableQuery]
        public SingleResult<Conversation> GetConversation([FromODataUri] int key)
        {
            Conversation conversation = conversationRepository.GetByID(key);
            return SingleResult.Create(new[] { conversation }.AsQueryable());
        }

        // PUT: odata/Conversation(5)
        public IHttpActionResult Put([FromODataUri] int key, Delta<Conversation> patch)
        {
            this.Validate(patch.GetEntity());
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Conversation conversation = conversationRepository.GetByID(key);
            if (conversation == null)
            {
                return NotFound();
            }

            Conversation updatedConversation = patch.GetEntity();
            updatedConversation.Id = key;
            conversationRepository.Update(conversation, updatedConversation);
            try
            {
                unitOfWork.Save();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ConversationExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: odata/Conversation
        public IHttpActionResult Post(Conversation conversation)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            Conversation databaseConversation = conversationRepository.Get(x =>
                                                        (x.User1 == conversation.User1 | x.User1 == conversation.User2) & 
                                                        (x.User2 == conversation.User1 | x.User2 == conversation.User2))
                                                        .FirstOrDefault();
            if (databaseConversation != null)
            {
                return BadRequest("Conversation already in the database");
            }
            conversationRepository.Insert(conversation);
            unitOfWork.Save();
            return StatusCode(HttpStatusCode.NoContent);
        }

        // PATCH: odata/Conversation(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<Conversation> patch)
        {
            Validate(patch.GetEntity());
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            Conversation conversation = conversationRepository.GetByID(key);
            if (conversation == null)
            {
                return NotFound();
            }
            Conversation updatedConversation = patch.GetEntity();
            updatedConversation.Id = key;
            conversationRepository.Update(conversation, updatedConversation);
            try
            {
                unitOfWork.Save();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ConversationExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return StatusCode(HttpStatusCode.NoContent);
        }

        // DELETE: odata/Conversation(5)
        public IHttpActionResult Delete([FromODataUri] int key)
        {
            Conversation conversation = conversationRepository.GetByID(key);
            if (conversation == null)
            {
                return NotFound();
            }
            conversationRepository.Delete(conversation);
            unitOfWork.Save();
            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET: odata/Conversation(5)/MessageList
        [EnableQuery]
        public IQueryable<Message> GetMessageList([FromODataUri] int key)
        {
            var conversation = conversationRepository.Get(x => x.Id == key, includeProperties: "MessageList");
            return conversation.SelectMany(x => x.MessageList);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ConversationExists(int key)
        {
            Conversation conversation = conversationRepository.GetByID(key);
            return (conversation != null) ? true : false;
        }
    }
}
