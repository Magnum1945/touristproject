﻿using System.Data;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.OData;
using LetsTravel.DAL;
using LetsTravel.DAL.Abstractions;
using LetsTravel.Entities;

namespace LetsTravel.Controllers.WebApi
{
    [Authorize]
    public class CityController : ODataController
    {
        private UnitOfWork unitOfWork;
        private GenericRepository<City> cityRepository;
        public CityController()
        {
            this.unitOfWork = new UnitOfWork();
            this.cityRepository = unitOfWork.GetRepository<City>();
        }
        public CityController(ITouristModel touristModel)
        {
            this.unitOfWork = new UnitOfWork(touristModel);
            this.cityRepository = unitOfWork.GetRepository<City>();
        }
        // GET: odata/City
        [EnableQuery]
        public IQueryable<City> GetCity()
        {
            return cityRepository.Get();
        }

        // GET: odata/City(5)
        [EnableQuery]
        public SingleResult<City> GetCity([FromODataUri] int key)
        {
            City city = cityRepository.GetByID(key);
            return SingleResult.Create(new[] { city }.AsQueryable());
        }

        // PUT: odata/City(5)
        public IHttpActionResult Put([FromODataUri] int key, Delta<City> patch)
        {
            Validate(patch.GetEntity());
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            City city = cityRepository.GetByID(key);
            if (city == null)
            {
                return NotFound();
            }
            City updatedCity = patch.GetEntity();
            updatedCity.Id = key;
            cityRepository.Update(city, updatedCity);
            try
            {
                unitOfWork.Save();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CityExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: odata/City
        public IHttpActionResult Post(City city)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            City databaseCity = cityRepository.Get(x=>x.Name.ToLower() == city.Name.ToLower()).FirstOrDefault();
            if (databaseCity != null)
            {
                return BadRequest("City already in the database");
            }
            cityRepository.Insert(city);
            unitOfWork.Save();
            return StatusCode(HttpStatusCode.NoContent);
        }

        // PATCH: odata/City(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<City> patch)
        {
            Validate(patch.GetEntity());
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            City city = cityRepository.GetByID(key);
            if (city == null)
            {
                return NotFound();
            }
            City updatedCity = patch.GetEntity();
            updatedCity.Id = key;
            cityRepository.Update(city, updatedCity);
            try
            {
                unitOfWork.Save();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CityExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return StatusCode(HttpStatusCode.NoContent);
        }

        // DELETE: odata/City(5)
        public IHttpActionResult Delete([FromODataUri] int key)
        {
            City city = cityRepository.GetByID(key);
            if (city == null)
            {
                return NotFound();
            }
            cityRepository.Delete(city);
            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET: odata/City(5)/PlaceList
        [EnableQuery]
        public IQueryable<Place> GetPlaceList([FromODataUri] int key)
        {
            var city = cityRepository.Get(x => x.Id == key, includeProperties: "PlaceList");
            return city.SelectMany(x => x.PlaceList);
        }

        // GET: odata/City(5)/State
        [EnableQuery]
        public SingleResult<State> GetState([FromODataUri] int key)
        {
            var city = cityRepository.Get(x => x.Id == key, includeProperties: "State");
            return SingleResult.Create(city.Select(x => x.State).AsQueryable());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CityExists(int key)
        {
            City city = cityRepository.GetByID(key);
            return (city != null) ? true : false;
        }
    }
}
