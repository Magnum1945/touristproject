﻿using System.Data;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.OData;
using LetsTravel.DAL;
using LetsTravel.DAL.Abstractions;
using LetsTravel.Entities;

namespace LetsTravel.Controllers.WebApi
{
    [Authorize]
    public class TripController : ODataController
    {
        private UnitOfWork unitOfWork;
        private GenericRepository<Trip> tripRepository;
        private GenericRepository<User> userRepository;
        public TripController()
        {
            this.unitOfWork = new UnitOfWork();
            this.tripRepository = unitOfWork.GetRepository<Trip>();
            this.userRepository = unitOfWork.GetRepository<User>();
        }
        public TripController(ITouristModel touristModel)
        {
            this.unitOfWork = new UnitOfWork(touristModel);
            this.tripRepository = unitOfWork.GetRepository<Trip>();
            this.userRepository = unitOfWork.GetRepository<User>();
        }

        // GET: odata/Trip
        [EnableQuery]
        public IQueryable<Trip> GetTrip()
        {
            return tripRepository.Get();
        }

        // GET: odata/Trip(5)
        [EnableQuery]
        public SingleResult<Trip> GetTrip([FromODataUri] int key)
        {
            Trip trip = tripRepository.GetByID(key);
            return SingleResult.Create(new[] { trip }.AsQueryable());
        }

        // PUT: odata/Trip(5)
        public IHttpActionResult Put([FromODataUri] int key, Delta<Trip> patch)
        {
            Validate(patch.GetEntity());
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            Trip trip = tripRepository.GetByID(key);
            if (trip == null)
            {
                return NotFound();
            }
            Trip updatedTrip = patch.GetEntity();
            updatedTrip.Id = key;
            tripRepository.Update(trip, updatedTrip);
            try
            {
                unitOfWork.Save();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TripExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: odata/Trip
        public IHttpActionResult Post(Trip trip)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            User databaseUser = userRepository.Get(x => x.Login == trip.UserLogin).FirstOrDefault();
            if (databaseUser == null)
            {
                return BadRequest("Wrong user");
            }
            tripRepository.Insert(trip);
            unitOfWork.Save();
            return StatusCode(HttpStatusCode.NoContent);
        }

        // PATCH: odata/Trip(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<Trip> patch)
        {
            Validate(patch.GetEntity());
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            Trip trip = tripRepository.GetByID(key);
            if (trip == null)
            {
                return NotFound();
            }
            Trip updatedTrip = patch.GetEntity();
            updatedTrip.Id = key;
            tripRepository.Update(trip, updatedTrip);
            try
            {
                unitOfWork.Save();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TripExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return StatusCode(HttpStatusCode.NoContent);
        }

        // DELETE: odata/Trip(5)
        public IHttpActionResult Delete([FromODataUri] int key)
        {
            Trip trip = tripRepository.GetByID(key);
            if (trip == null)
            {
                return NotFound();
            }
            tripRepository.Delete(trip);
            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET: odata/Trip(5)/PlaceList
        [EnableQuery]
        public IQueryable<Place> GetPlaceList([FromODataUri] int key)
        {
            var trip = tripRepository.Get(x => x.Id == key, includeProperties: "PlaceList");
            return trip.SelectMany(x => x.PlaceList);
        }

        // GET: odata/Trip(5)/PostList
        [EnableQuery]
        public IQueryable<Post> GetPostList([FromODataUri] int key)
        {
            var trip = tripRepository.Get(x => x.Id == key, includeProperties: "PostList");
            return trip.SelectMany(x => x.PostList);
        }

        // GET: odata/Trip(5)/UserList
        [EnableQuery]
        public IQueryable<User> GetUserList([FromODataUri] int key)
        {
            var trip = tripRepository.Get(x => x.Id == key, includeProperties: "UserList");
            return trip.SelectMany(x => x.UserList);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TripExists(int key)
        {
            Trip trip = tripRepository.GetByID(key);
            return (trip != null) ? true : false;
        }
    }
}
