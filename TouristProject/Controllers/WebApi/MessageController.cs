﻿using System.Data;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.OData;
using LetsTravel.DAL;
using LetsTravel.DAL.Abstractions;
using LetsTravel.Entities;

namespace LetsTravel.Controllers.WebApi
{
    [Authorize]
    public class MessageController : ODataController
    {
        private UnitOfWork unitOfWork;
        private GenericRepository<Message> messageRepository;
        private GenericRepository<Conversation> conversationRepository;
        public MessageController()
        {
            this.unitOfWork = new UnitOfWork();
            this.messageRepository = unitOfWork.GetRepository<Message>();
            this.conversationRepository = unitOfWork.GetRepository<Conversation>();
        }
        public MessageController(ITouristModel touristModel)
        {
            this.unitOfWork = new UnitOfWork(touristModel);
            this.messageRepository = unitOfWork.GetRepository<Message>();
            this.conversationRepository = unitOfWork.GetRepository<Conversation>();
        }

        // GET: odata/Message
        [EnableQuery]
        public IQueryable<Message> GetMessage()
        {
            return messageRepository.Get();
        }

        // GET: odata/Message(5)
        [EnableQuery]
        public SingleResult<Message> GetMessage([FromODataUri] int key)
        {
            Message message = messageRepository.GetByID(key);
            return SingleResult.Create(new[] { message }.AsQueryable());
        }

        // PUT: odata/Message(5)
        public IHttpActionResult Put([FromODataUri] int key, Delta<Message> patch)
        {
            Validate(patch.GetEntity());
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            Message message = messageRepository.GetByID(key);
            if (message == null)
            {
                return NotFound();
            }
            Message updatedMessage = patch.GetEntity();
            updatedMessage.Id = key;
            messageRepository.Update(message, updatedMessage);
            try
            {
                unitOfWork.Save();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MessageExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: odata/Message
        public IHttpActionResult Post(Message message)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            Conversation databaseConversation = conversationRepository.Get(x => x.Id == message.ConversationId).FirstOrDefault();
            if (databaseConversation == null)
            {
                return BadRequest("Conversation is missing in the database");
            }
            if (!(databaseConversation.User1 == message.Sender | databaseConversation.User2 == message.Sender))
            {
                return BadRequest("Wrong sender");
            }
            messageRepository.Insert(message);
            unitOfWork.Save();
            return StatusCode(HttpStatusCode.NoContent);
        }

        // PATCH: odata/Message(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<Message> patch)
        {
            Validate(patch.GetEntity());
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            Message message = messageRepository.GetByID(key);
            if (message == null)
            {
                return NotFound();
            }
            Message updatedMessage = patch.GetEntity();
            updatedMessage.Id = key;
            messageRepository.Update(message, updatedMessage);
            try
            {
                unitOfWork.Save();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MessageExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return StatusCode(HttpStatusCode.NoContent);
        }

        // DELETE: odata/Message(5)
        public IHttpActionResult Delete([FromODataUri] int key)
        {
            Message message = messageRepository.GetByID(key);
            if (message == null)
            {
                return NotFound();
            }
            messageRepository.Delete(message);
            unitOfWork.Save();
            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET: odata/Message(5)/Conversation
        [EnableQuery]
        public SingleResult<Conversation> GetConversation([FromODataUri] int key)
        {
            var conversation = messageRepository.Get(x => x.Id == key, includeProperties: "Conversation");
            return SingleResult.Create(conversation.Select(x => x.Conversation).AsQueryable());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MessageExists(int key)
        {
            Message message = messageRepository.GetByID(key);
            return (message != null) ? true : false;
        }
    }
}
