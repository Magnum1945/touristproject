﻿using System.Data;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.OData;
using LetsTravel.DAL;
using LetsTravel.DAL.Abstractions;
using LetsTravel.Entities;

namespace LetsTravel.Controllers.WebApi
{
    [Authorize]
    public class AddressController : ODataController
    {
        private UnitOfWork unitOfWork;
        private GenericRepository<Address> addressRepository;

        public AddressController()
        {
            this.unitOfWork = new UnitOfWork();
            this.addressRepository = unitOfWork.GetRepository<Address>();
        }

        public AddressController(ITouristModel touristModel)
        {
            this.unitOfWork = new UnitOfWork(touristModel);
            this.addressRepository = unitOfWork.GetRepository<Address>();
        }

        // GET: odata/Address
        [EnableQuery]
        public IQueryable<Address> GetAddress()
        {
            return addressRepository.Get();
        }

        // GET: odata/Address(5)
        [EnableQuery]
        public SingleResult<Address> GetAddress([FromODataUri] int key)
        {
            Address address = addressRepository.GetByID(key);
            return SingleResult.Create(new[] { address }.AsQueryable());
        }

        // PUT: odata/Address(5)
        public IHttpActionResult Put([FromODataUri] int key, Delta<Address> patch)
        {
            Validate(patch.GetEntity());
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            Address address = addressRepository.GetByID(key);
            if (address == null)
            {
                return NotFound();
            }

            Address updatedAddress = patch.GetEntity();
            updatedAddress.Id = key;
            addressRepository.Update(address, updatedAddress);
            try
            {
                unitOfWork.Save();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AddressExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: odata/Address
        public IHttpActionResult Post(Address address)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            addressRepository.Insert(address);
            try
            {
                unitOfWork.Save();
            }
            catch (DbUpdateException)
            {
                if (AddressExists(address.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // PATCH: odata/Address(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<Address> patch)
        {
            Validate(patch.GetEntity());
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Address address = addressRepository.GetByID(key);
            if (address == null)
            {
                return NotFound();
            }

            Address updatedAddress = patch.GetEntity();
            updatedAddress.Id = key;
            addressRepository.Update(address, updatedAddress);
            try
            {
                unitOfWork.Save();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AddressExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // DELETE: odata/Address(5)
        public IHttpActionResult Delete([FromODataUri] int key)
        {
            Address address = addressRepository.GetByID(key);
            if (address == null)
            {
                return NotFound();
            }

            addressRepository.Delete(address);
            unitOfWork.Save();
            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET: odata/Address(5)/User
        [EnableQuery]
        public SingleResult<User> GetUser([FromODataUri] int key)
        {
            var address = addressRepository.Get(x => x.Id == key, includeProperties: "User");
            return SingleResult.Create(address.Select(x => x.User).AsQueryable());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }

            base.Dispose(disposing);
        }

        private bool AddressExists(int key)
        {
            Address address = addressRepository.GetByID(key);
            return (address != null) ? true : false;
        }
    }
}
