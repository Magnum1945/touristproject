﻿using System.Data;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.OData;
using LetsTravel.DAL;
using LetsTravel.DAL.Abstractions;
using LetsTravel.Entities;

namespace LetsTravel.Controllers.WebApi
{
    [Authorize]
    public class PostController : ODataController
    {
        private UnitOfWork unitOfWork;
        private GenericRepository<Post> postRepository;
        private GenericRepository<User> userRepository;
        public PostController()
        {
            this.unitOfWork = new UnitOfWork();
            this.postRepository = unitOfWork.GetRepository<Post>();
            this.userRepository = unitOfWork.GetRepository<User>();
        }
        public PostController(ITouristModel touristModel)
        {
            this.unitOfWork = new UnitOfWork(touristModel);
            this.postRepository = unitOfWork.GetRepository<Post>();
            this.userRepository = unitOfWork.GetRepository<User>();
        }

        // GET: odata/Post
        [EnableQuery]
        public IQueryable<Post> GetPost()
        {
            return postRepository.Get();
        }

        // GET: odata/Post(5)
        [EnableQuery]
        public SingleResult<Post> GetPost([FromODataUri] int key)
        {
            Post post = postRepository.GetByID(key);
            return SingleResult.Create(new[] { post }.AsQueryable());
        }

        // PUT: odata/Post(5)
        public IHttpActionResult Put([FromODataUri] int key, Delta<Post> patch)
        {
            Validate(patch.GetEntity());
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            Post post = postRepository.GetByID(key);
            if (post == null)
            {
                return NotFound();
            }
            Post updatedPost = patch.GetEntity();
            updatedPost.Id = key;
            postRepository.Update(post, updatedPost);
            try
            {
                unitOfWork.Save();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PostExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: odata/Post
        public IHttpActionResult Post(Post post)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            User databaseUser = userRepository.Get(x => x.Login == post.UserLogin).FirstOrDefault();
            if (databaseUser == null)
            {
                return BadRequest("Wrong user");
            }
            postRepository.Insert(post);
            unitOfWork.Save();
            return StatusCode(HttpStatusCode.NoContent);
        }

        // PATCH: odata/Post(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<Post> patch)
        {
            Validate(patch.GetEntity());
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            Post post = postRepository.GetByID(key);
            if (post == null)
            {
                return NotFound();
            }
            Post updatedPost = patch.GetEntity();
            updatedPost.Id = key;
            postRepository.Update(post, updatedPost);
            try
            {
                unitOfWork.Save();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PostExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return StatusCode(HttpStatusCode.NoContent);
        }

        // DELETE: odata/Post(5)
        public IHttpActionResult Delete([FromODataUri] int key)
        {
            Post post = postRepository.GetByID(key);
            if (post == null)
            {
                return NotFound();
            }
            postRepository.Delete(post);
            unitOfWork.Save();
            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET: odata/Post(5)/Trip
        [EnableQuery]
        public SingleResult<Trip> GetTrip([FromODataUri] int key)
        {
            var post = postRepository.Get(x => x.Id == key, includeProperties: "Trip");
            return SingleResult.Create(post.Select(x => x.Trip).AsQueryable());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PostExists(int key)
        {
            Post post = postRepository.GetByID(key);
            return (post != null) ? true : false;
        }
    }
}
