﻿using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.OData;
using LetsTravel.DAL;
using LetsTravel.DAL.Abstractions;
using LetsTravel.Entities;

namespace LetsTravel.Controllers.WebApi
{
    [Authorize]
    public class LanguageController : ODataController
    {
        private UnitOfWork unitOfWork;
        private GenericRepository<Language> languageRepository;
        public LanguageController()
        {
            this.unitOfWork = new UnitOfWork();
            this.languageRepository = unitOfWork.GetRepository<Language>();
        }
        public LanguageController(ITouristModel touristModel)
        {
            this.unitOfWork = new UnitOfWork(touristModel);
            this.languageRepository = unitOfWork.GetRepository<Language>();
        }

        // GET: odata/Language
        [EnableQuery]
        public IQueryable<Language> GetLanguage()
        {
            return languageRepository.Get();
        }
        
        // GET: odata/Language(5)
        [EnableQuery]
        public SingleResult<Language> GetLanguage([FromODataUri] int key)
        {
            var language = languageRepository.GetByID(key);
            return SingleResult.Create(new[] { language }.AsQueryable());
        }
        
        // PUT: odata/Language(5)
        public IHttpActionResult Put([FromODataUri] int key, Delta<Language> patch)
        {
            Validate(patch.GetEntity());
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            Language language = languageRepository.GetByID(key);
            if (language == null)
            {
                return NotFound();
            }
            Language updatedLanguage = patch.GetEntity();
            updatedLanguage.Id = key;
            languageRepository.Update(language,updatedLanguage);
            try
            {
                unitOfWork.Save();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LanguageExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return StatusCode(HttpStatusCode.NoContent);
        }
        
        // POST: odata/Language
        public IHttpActionResult Post(Language language)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            Language databaseLanguage = languageRepository.Get(x => x.Name.ToLower() == language.Name.ToLower()).FirstOrDefault();
            if (databaseLanguage != null)
            {
                return BadRequest("Language already in the database");
            }
            languageRepository.Insert(language);
            unitOfWork.Save();
            return StatusCode(HttpStatusCode.NoContent);
        }
        
        // PATCH: odata/Language(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<Language> patch)
        {
            Validate(patch.GetEntity());
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            Language language = languageRepository.GetByID(key);
            if (language == null)
            {
                return NotFound();
            }
            Language updatedLanguage = patch.GetEntity();
            updatedLanguage.Id = key;
            languageRepository.Update(language, updatedLanguage);
            try
            {
                unitOfWork.Save();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LanguageExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return StatusCode(HttpStatusCode.NoContent);
        }
        
        // DELETE: odata/Language(5)
        public IHttpActionResult Delete([FromODataUri] int key)
        {
            Language language = languageRepository.GetByID(key);
            if (language == null)
            {
                return NotFound();
            }
            languageRepository.Delete(language);
            unitOfWork.Save();
            return StatusCode(HttpStatusCode.NoContent);
        }
        
        // GET: odata/Language(5)/UserList
        [EnableQuery]
        public IQueryable<User> GetUserList([FromODataUri] int key)
        {
            var language = languageRepository.Get(x => x.Id == key, includeProperties: "UserList");
            return language.SelectMany(x => x.UserList);
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }
        private bool LanguageExists(int key)
        {
            Language language = languageRepository.GetByID(key);
            return (language != null) ? true : false;
        }
    }
}
