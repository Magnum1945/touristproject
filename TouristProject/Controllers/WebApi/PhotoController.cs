﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using LetsTravel.DAL;
using LetsTravel.DAL.Abstractions;
using LetsTravel.Entities;
using LetsTravel.Entities.GoogleMapsModels;
using LetsTravel.Helpers;

namespace LetsTravel.Controllers.WebApi
{
    [Authorize]
    [RoutePrefix("api/Photo")]
    public class PhotoController : ApiController
    {
        private UnitOfWork unitOfWork;
        private GenericRepository<Place> placeRepository;
        private GenericRepository<Trip> tripRepository;
        private GenericRepository<User> userRepository;
        private GenericRepository<City> cityRepository;
        private GenericRepository<State> stateRepository;
        private GenericRepository<Country> countryRepository;
        private IPathProvider pathProvider;
        public PhotoController()
        {
            this.unitOfWork = new UnitOfWork();
            this.placeRepository = unitOfWork.GetRepository<Place>();
            this.tripRepository = unitOfWork.GetRepository<Trip>();
            this.userRepository = unitOfWork.GetRepository<User>();
            this.cityRepository = unitOfWork.GetRepository<City>();
            this.stateRepository = unitOfWork.GetRepository<State>();
            this.countryRepository = unitOfWork.GetRepository<Country>();
            this.pathProvider = new ServerPathProvider();
        }
        public PhotoController(ITouristModel touristModel, IPathProvider pathProvider)
        {
            this.unitOfWork = new UnitOfWork(touristModel);
            this.placeRepository = unitOfWork.GetRepository<Place>();
            this.tripRepository = unitOfWork.GetRepository<Trip>();
            this.userRepository = unitOfWork.GetRepository<User>();
            this.cityRepository = unitOfWork.GetRepository<City>();
            this.stateRepository = unitOfWork.GetRepository<State>();
            this.countryRepository = unitOfWork.GetRepository<Country>();
            this.pathProvider = pathProvider;
        }

        #region GET ALL
        // GET: api/Photo/City?id=1
        [Route("City")]
        public HttpResponseMessage Get(int id)
        {
            try
            {
                List<Place> placesList = placeRepository.Get(x => x.CityId == id).ToList();
                if (placesList.Count != 0)
                {
                    MultipartContent content = new MultipartContent();
                    foreach (Place place in placesList)
                    {
                        //string path = System.IO.Path.Combine(HttpRuntime.AppDomainAppPath, place.Photo);
                        string path = pathProvider.MapPath(place.Photo);
                        StreamContent fileContent = new StreamContent(new FileStream(path, FileMode.Open));
                        fileContent.Headers.ContentType = MediaTypeHeaderValue.Parse("image/jpeg");
                        content.Add(fileContent);
                    }
                    HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
                    response.Content = content;
                    return response;
                }
                else
                    return new HttpResponseMessage(HttpStatusCode.NotFound);
            }
            catch (Exception)
            {
                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }
        }
        #endregion

        #region GET WITH ID
        // GET: api/Photo/Place?id=1
        [Route("Place")]
        public HttpResponseMessage GetPlacePhoto(int id)
        {
            Place place = placeRepository.GetByID(id);
            if (place != null)
                return CheckAndDownloadPhoto(place.Photo);
            else
                return new HttpResponseMessage(HttpStatusCode.NotFound);
        }

        // GET: api/Photo/User?id=1
        [Route("User")]
        public HttpResponseMessage GetUserPhoto(int id)
        {
            User user = userRepository.GetByID(id);
            if (user != null)
                return CheckAndDownloadPhoto(user.Photo);
            else
                return new HttpResponseMessage(HttpStatusCode.NotFound);
        }

        // GET: api/Photo/Trip?id=1
        [Route("Trip")]
        public HttpResponseMessage GetTripPhoto(int id)
        {
            Trip trip = tripRepository.GetByID(id);
            if (trip != null)
                return CheckAndDownloadPhoto(trip.Photo);
            else
                return new HttpResponseMessage(HttpStatusCode.NotFound);
        }
        #endregion

        #region POST
        // POST: api/Photo/Place?id=1
        [Route("Place")]
        public HttpResponseMessage PostPlacePhoto(int id)
        {
            Place place = placeRepository.GetByID(id);
            if (place != null)
            {
                GoogleAddress address = GetPlaceAddress(place);
                if (address != null)
                {
                    //if (!Request.Content.IsMimeMultipartContent())
                    //{
                    //    throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
                    //}
                    HttpRequest httpRequest = HttpContext.Current.Request;
                    if (httpRequest.Files.Count == 1)
                    {
                        string uploadedFile = string.Empty;
                        foreach (string file in httpRequest.Files)
                        {
                            var postedFile = httpRequest.Files[file];
                            string fileName = postedFile.FileName;
                            string fileExtension = System.IO.Path.GetExtension(fileName);
                            if (CheckFileExtension(fileExtension))
                            {
                                string filePath = $"/Content/Images/Places/{address.Country}/{address.State}/{address.City}/{place.GooglePlaceId}{fileExtension}";
                                if (SaveFile(postedFile, filePath))
                                {
                                    place.Photo = filePath;
                                    placeRepository.Update(place);
                                    unitOfWork.Save();
                                }
                                else
                                    return new HttpResponseMessage(HttpStatusCode.BadRequest);
                            }
                            else
                                return new HttpResponseMessage(HttpStatusCode.BadRequest);
                        }
                        HttpResponseMessage result = Request.CreateResponse(HttpStatusCode.Created, uploadedFile);
                        return result;
                    }
                    else
                        return new HttpResponseMessage(HttpStatusCode.BadRequest);
                }
                else
                    return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
            else
                return new HttpResponseMessage(HttpStatusCode.NotFound);
        }

        // POST: api/Photo/User?id=1
        [Route("User")]
        public HttpResponseMessage PostUserPhoto(int id)
        {
            User user = userRepository.GetByID(id);
            if (user != null)
            {
                HttpRequest httpRequest = HttpContext.Current.Request;
                if (httpRequest.Files.Count == 1)
                {
                    string uploadedFile = string.Empty;
                    foreach (string file in httpRequest.Files)
                    {
                        var postedFile = httpRequest.Files[file];
                        string fileName = postedFile.FileName;
                        string fileExtension = System.IO.Path.GetExtension(fileName);
                        if (CheckFileExtension(fileExtension))
                        {
                            string filePath = $"/Content/Images/Users/{user.Id}.{fileExtension}";
                            if (SaveFile(postedFile, filePath))
                            {
                                user.Photo = filePath;
                                userRepository.Update(user);
                                unitOfWork.Save();
                            }
                            else
                                return new HttpResponseMessage(HttpStatusCode.BadRequest);
                        }
                        else
                            return new HttpResponseMessage(HttpStatusCode.BadRequest);
                    }
                    HttpResponseMessage result = Request.CreateResponse(HttpStatusCode.Created, uploadedFile);
                    return result;
                }
                else
                    return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
            else
                return new HttpResponseMessage(HttpStatusCode.NotFound);
        }

        // POST: api/Photo/Trip?id=1
        [Route("Trip")]
        public HttpResponseMessage PostTripPhoto(int id)
        {
            Trip trip = tripRepository.GetByID(id);
            if (trip != null)
            {
                HttpRequest httpRequest = HttpContext.Current.Request;
                if (httpRequest.Files.Count == 1)
                {
                    string uploadedFile = string.Empty;
                    foreach (string file in httpRequest.Files)
                    {
                        var postedFile = httpRequest.Files[file];
                        string fileName = postedFile.FileName;
                        string fileExtension = System.IO.Path.GetExtension(fileName);
                        if (CheckFileExtension(fileExtension))
                        {
                            string filePath = $"/Content/Images/Trip/{trip.CountryName}/{trip.StateName}/{trip.CityName}/{trip.Id}.{fileExtension}";
                            if (SaveFile(postedFile, filePath))
                            {
                                trip.Photo = filePath;
                                tripRepository.Update(trip);
                                unitOfWork.Save();
                            }
                            else
                                return new HttpResponseMessage(HttpStatusCode.BadRequest);
                        }
                        else
                            return new HttpResponseMessage(HttpStatusCode.BadRequest);
                    }
                    HttpResponseMessage result = Request.CreateResponse(HttpStatusCode.Created, uploadedFile);
                    return result;
                }
                else
                    return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
            else
                return new HttpResponseMessage(HttpStatusCode.NotFound);
        }
        #endregion

        #region PUT
        // PUT: api/Photo/Place?id=1
        //public HttpResponseMessage PutPlacePhoto(int id)
        //{
        //}
        #endregion

        #region DELETE
        // DELETE: api/Photo/Place?id=1
        [Route("Place")]
        public HttpResponseMessage DeletePlacePhoto(int id)
        {
            Place place = placeRepository.GetByID(id);
            if (place != null)
            {
                HttpResponseMessage response = DeletePhoto(place.Photo);
                place.Photo = "/Content/Images/No-image-available.jpg";
                placeRepository.Update(place);
                unitOfWork.Save();
                return response;
            }
            else
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }
        }

        // DELETE: api/Photo/User?id=1
        [Route("User")]
        public HttpResponseMessage DeleteUserPhoto(int id)
        {
            User user = userRepository.GetByID(id);
            if (user != null)
            {
                HttpResponseMessage response = DeletePhoto(user.Photo);
                user.Photo = "/Content/Images/No-image-available.jpg";
                userRepository.Update(user);
                unitOfWork.Save();
                return response;
            }
            else
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }
        }

        // DELETE: api/Photo/Trip?id=1
        [Route("Trip")]
        public HttpResponseMessage DeleteTripPhoto(int id)
        {
            Trip trip = tripRepository.GetByID(id);
            if (trip != null)
            {
                HttpResponseMessage response = DeletePhoto(trip.Photo);
                trip.Photo = "/Content/Images/No-image-available.jpg";
                tripRepository.Update(trip);
                unitOfWork.Save();
                return response;
            }
            else
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }
        }
        #endregion

        public HttpResponseMessage CheckAndDownloadPhoto(string photoPath)
        {
            string path = pathProvider.MapPath(photoPath);
            if (!File.Exists(path))
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }
            try
            {
                byte[] fileData = File.ReadAllBytes(path);
                if (fileData == null)
                {
                    return new HttpResponseMessage(HttpStatusCode.NotFound);
                }
                else
                {
                    HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
                    response.Content = new ByteArrayContent(fileData);
                    response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                    return response;
                }
            }
            catch (IOException)
            {
                return CheckAndDownloadPhoto(photoPath);
            }

        }
        public HttpResponseMessage DeletePhoto(string photoPath)
        {
            string path = pathProvider.MapPath(photoPath);
            try
            {
                if (File.Exists(path))
                {
                    File.Delete(path);
                    return new HttpResponseMessage(HttpStatusCode.OK);
                }
                else
                    return new HttpResponseMessage(HttpStatusCode.NoContent);
            }
            catch (Exception)
            {
                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }

        }
        public GoogleAddress GetPlaceAddress(Place place)
        {
            GoogleAddress address = new GoogleAddress();
            City city = cityRepository.GetByID(place.CityId);
            if (city != null & city.Name != null & city.Name != string.Empty)
            {
                State state = stateRepository.GetByID(city.StateId);
                if (state != null & state.Name != null & state.Name != string.Empty)
                {
                    Country country = countryRepository.GetByID(state.CountryId);
                    if (country != null & country.Name != null & country.Name != string.Empty)
                    {
                        address.City = city.Name;
                        address.State = state.Name;
                        address.Country = country.Name;
                    }
                }
            }
            return address;
        }
        public bool SaveFile(HttpPostedFile postedFile, string filePath)
        {
            try
            {
                string path = pathProvider.MapPath(filePath);
                FileInfo fi = new FileInfo(path);
                if (fi.Directory.Exists == false)
                    fi.Directory.Create();
                postedFile.SaveAs(path);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool CheckFileExtension(string fileExtension)
        {
            switch (fileExtension.ToLower())
            {
                case ".jpg":
                case ".png":
                case ".gif":
                    return true;
                default:
                    return false;
            }
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
