﻿using System.Data;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.OData;
using LetsTravel.Entities;
using LetsTravel.DAL;
using LetsTravel.DAL.Abstractions;

namespace LetsTravel.Controllers.WebApi
{
    [Authorize]
    public class UserController : ODataController
    {
        private UnitOfWork unitOfWork;
        private GenericRepository<User> userRepository;
        public UserController()
        {
            this.unitOfWork = new UnitOfWork();
            this.userRepository = unitOfWork.GetRepository<User>();
        }
        public UserController(ITouristModel touristModel)
        {
            this.unitOfWork = new UnitOfWork(touristModel);
            this.userRepository = unitOfWork.GetRepository<User>();
        }

        // GET: odata/User
        [EnableQuery]
        public IQueryable<User> GetUser()
        {
            return userRepository.Get();
        }
        
        // GET: odata/User(5)
        [EnableQuery]
        public SingleResult<User> GetUser([FromODataUri] int key)
        {
            User user = userRepository.GetByID(key);
            return SingleResult.Create(new[] { user }.AsQueryable());
        }
        
        // PUT: odata/User(5)
        public IHttpActionResult Put([FromODataUri] int key, Delta<User> patch)
        {
            Validate(patch.GetEntity());
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            User user = userRepository.GetByID(key);
            if (user == null)
            {
                return NotFound();
            }
            User updatedUser = patch.GetEntity();
            updatedUser.Id = key;
            userRepository.Update(user, updatedUser);
            try
            {
                unitOfWork.Save();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return StatusCode(HttpStatusCode.NoContent);
        }
        
        // POST: odata/User
        public IHttpActionResult Post(User user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            User databaseCity = userRepository.Get(x => x.Email == user.Email).FirstOrDefault();
            if (databaseCity != null)
            {
                return BadRequest("User already in the database");
            }
            userRepository.Insert(user);
            unitOfWork.Save();
            return StatusCode(HttpStatusCode.NoContent);
        }
        
        // PATCH: odata/User(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<User> patch)
        {
            Validate(patch.GetEntity());
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            User user = userRepository.GetByID(key);
            if (user == null)
            {
                return NotFound();
            }
            User updatedUser = patch.GetEntity();
            updatedUser.Id = key;
            userRepository.Update(user, updatedUser);
            try
            {
                unitOfWork.Save();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return StatusCode(HttpStatusCode.NoContent);
        }
        
        // DELETE: odata/User(5)
        public IHttpActionResult Delete([FromODataUri] int key)
        {
            User user = userRepository.GetByID(key);
            if (user == null)
            {
                return NotFound();
            }
            userRepository.Delete(user);
            unitOfWork.Save();
            return StatusCode(HttpStatusCode.NoContent);
        }
        
        // GET: odata/User(5)/Address
        [EnableQuery]
        public SingleResult<Address> GetAddress([FromODataUri] int key)
        {
            var user = userRepository.Get(x => x.Id == key, includeProperties: "Address");
            return SingleResult.Create(user.Select(x => x.Address).AsQueryable());
        }
        
        // GET: odata/User(5)/LanguageList
        [EnableQuery]
        public IQueryable<Language> GetLanguageList([FromODataUri] int key)
        {
            var user = userRepository.Get(x => x.Id == key, includeProperties: "LanguageList");
            return user.SelectMany(x => x.LanguageList);
        }
        
        // GET: odata/User(5)/TripList
        [EnableQuery]
        public IQueryable<Trip> GetTripList([FromODataUri] int key)
        {
            var user = userRepository.Get(x => x.Id == key, includeProperties: "TripList");
            return user.SelectMany(x=>x.TripList);
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }
        private bool UserExists(int key)
        {
            User user = userRepository.GetByID(key);
            return (user != null) ? true : false;
        }
    }
}
