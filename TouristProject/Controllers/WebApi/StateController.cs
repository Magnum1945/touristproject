﻿using System.Data;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.OData;
using LetsTravel.Entities;
using LetsTravel.DAL;
using LetsTravel.DAL.Abstractions;

namespace LetsTravel.Controllers.WebApi
{
    [Authorize]
    public class StateController : ODataController
    {
        private UnitOfWork unitOfWork;
        private GenericRepository<State> stateRepository;
        public StateController()
        {
            this.unitOfWork = new UnitOfWork();
            this.stateRepository = unitOfWork.GetRepository<State>();
        }
        public StateController(ITouristModel touristModel)
        {
            this.unitOfWork = new UnitOfWork(touristModel);
            this.stateRepository = unitOfWork.GetRepository<State>();
        }
        // GET: odata/State
        [EnableQuery]
        public IQueryable<State> GetState()
        {
            return stateRepository.Get();
        }

        // GET: odata/State(5)
        [EnableQuery]
        public SingleResult<State> GetState([FromODataUri] int key)
        {
            State state = stateRepository.GetByID(key);
            return SingleResult.Create(new[] { state }.AsQueryable());
        }

        // PUT: odata/State(5)
        public IHttpActionResult Put([FromODataUri] int key, Delta<State> patch)
        {
            Validate(patch.GetEntity());
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            State state = stateRepository.GetByID(key);
            if (state == null)
            {
                return NotFound();
            }
            State updatedState = patch.GetEntity();
            updatedState.Id = key;
            stateRepository.Update(state, updatedState);
            try
            {
                unitOfWork.Save();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!StateExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: odata/State
        public IHttpActionResult Post(State state)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            State databaseState = stateRepository.Get(x => x.Name.ToLower() == state.Name.ToLower()).FirstOrDefault();
            if (databaseState != null)
            {
                return BadRequest("State already in the database");
            }
            stateRepository.Insert(state);
            unitOfWork.Save();
            return StatusCode(HttpStatusCode.NoContent);
        }

        // PATCH: odata/State(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<State> patch)
        {
            Validate(patch.GetEntity());
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            State state = stateRepository.GetByID(key);
            if (state == null)
            {
                return NotFound();
            }
            State updatedState = patch.GetEntity();
            updatedState.Id = key;
            stateRepository.Update(state, updatedState);
            try
            {
                unitOfWork.Save();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!StateExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return StatusCode(HttpStatusCode.NoContent);
        }

        // DELETE: odata/State(5)
        public IHttpActionResult Delete([FromODataUri] int key)
        {
            State state = stateRepository.GetByID(key);
            if (state == null)
            {
                return NotFound();
            }
            stateRepository.Delete(state);
            unitOfWork.Save();
            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET: odata/State(5)/CityList
        [EnableQuery]
        public IQueryable<City> GetCityList([FromODataUri] int key)
        {
            var state = stateRepository.Get(x => x.Id == key, includeProperties: "CityList");
            return state.SelectMany(x => x.CityList);
        }

        // GET: odata/State(5)/Country
        [EnableQuery]
        public SingleResult<Country> GetCountry([FromODataUri] int key)
        {
            var state = stateRepository.Get(x => x.Id == key, includeProperties: "Country");
            return SingleResult.Create(state.Select(x => x.Country).AsQueryable());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool StateExists(int key)
        {
            State state = stateRepository.GetByID(key);
            return (state != null) ? true : false;
        }
    }
}
