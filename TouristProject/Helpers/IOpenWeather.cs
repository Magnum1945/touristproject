﻿using LetsTravel.Entities.OpenWeatherModels;

namespace LetsTravel.Helpers
{
    public interface IOpenWeather
    {
        string GetResult(string url);
        string GetForecastData(string city);
        OpenForecast.Example ParseForecastData(string forecastData);
    }
}
