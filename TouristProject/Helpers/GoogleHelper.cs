﻿using System.Configuration;
using System.Net;
using System.Text;
using LetsTravel.Entities.GoogleMapsModels;
using Newtonsoft.Json;
using LetsTravel.Entities;

namespace LetsTravel.Helpers
{
    public class GoogleHelper
    {
        public string DownloadPage(string url)
        {
            string result = string.Empty;
            using (WebClient client = new WebClient())
            {
                result = Encoding.UTF8.GetString(client.DownloadData(url));
            }
            return result;
        }
        public Place DownloadPlaceDetails(string placeId)
        {
            string googleApiKey = ConfigurationManager.AppSettings["googleApiKey"];
            string url = $"https://maps.googleapis.com/maps/api/place/details/json?placeid={placeId}&key={googleApiKey}";
            string result = DownloadPage(url);
            GooglePlaceDetails.Example placeDetails = JsonConvert.DeserializeObject<GooglePlaceDetails.Example>(result);
            Place place = new Place
            {
                FormattedAddress = placeDetails.result.formatted_address,
                InternationalPhoneNumber = GetInternationalPhoneNumber(placeDetails),
                GooglePlaceId = placeDetails.result.place_id,
                Latitude = placeDetails.result.geometry.location.lat,
                Longitude = placeDetails.result.geometry.location.lng,
                Name = placeDetails.result.name,
                OpeningHours = GetOpeningHours(placeDetails),
                Types = GetPlaceTypes(placeDetails),
                Rating = placeDetails.result.rating,
                Website = placeDetails.result.url,
                Reference = placeDetails.result.reference,
            };
            return place;
        }
        public string GetPlaceTypes(GooglePlaceDetails.Example placeDetails)
        {
            string types = string.Empty;
            foreach (var type in placeDetails.result.types)
            {
                types += $"{type}; ";
            }
            return types;
        }
        public string GetOpeningHours(GooglePlaceDetails.Example placeDetails)
        {
            string openingHours = string.Empty;
            if (placeDetails.result.opening_hours != null)
            {
                foreach (var opening in placeDetails.result.opening_hours.weekday_text)
                {
                    openingHours += $"{opening}; ";
                }
            }
            else
            {
                openingHours = "No data available!";
            }
            return openingHours;
        }
        public string GetInternationalPhoneNumber(GooglePlaceDetails.Example placeDetails)
        {
            string internationalPhoneNumber = string.Empty;
            if (placeDetails.result.international_phone_number != null)
            {
                internationalPhoneNumber = placeDetails.result.international_phone_number;
            }
            else
            {
                internationalPhoneNumber = "No data available!";
            }
            return internationalPhoneNumber;
        }


        #region OldCode
        //        public async Task<string> DownloadPageAsync(string url)
        //        {
        //            Debug.WriteLine($"Load place in DownloadPageAsync - {DateTime.Now}");

        //            string result = string.Empty;
        //            using (WebClient client = new WebClient())
        //            {
        //                result = Encoding.UTF8.GetString(await client.DownloadDataTaskAsync(url));
        //            }
        //            return result;
        //        }
        //        public async Task DownloadFileAsync(string url, string filePath)
        //        {
        //            try
        //            {
        //                using (WebClient wc = new WebClient())
        //                {
        //                    await wc.DownloadFileTaskAsync(url, filePath);
        //                }
        //            }
        //            catch (WebException ex)
        //            {
        //                Debug.WriteLine(ex);
        //            }
        //            catch (Exception ex)
        //            {
        //                Debug.WriteLine(ex);
        //            }
        //        }
        //        public void CheckForDirectoryAndDownloadFile(string photoUrl, string path)
        //        {
        //            try
        //            {
        //                FileInfo fi = new FileInfo(path);
        //                if (fi.Directory.Exists == false)
        //                {
        //                    fi.Directory.Create();
        //                    var task = Task.Run(async () => await DownloadFileAsync(photoUrl, path));
        //                    task.Wait();
        //                }
        //                else if (fi.Exists == false)
        //                {
        //                    var task = Task.Run(async () => await DownloadFileAsync(photoUrl, path));
        //                    task.Wait();
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                Debug.WriteLine(ex);
        //            }
        //        }
        //        public async Task<List<string>> GetResultsAsync(GooglePlace.Example placeObject)
        //        {
        //            List<Task<string>> downloadTasks = new List<Task<string>>();
        //            string googleApiKey = ConfigurationManager.AppSettings["googleApiKey"];
        //            foreach (var place in placeObject.results)
        //            {
        //                downloadTasks.Add(DownloadPageAsync($"https://maps.googleapis.com/maps/api/place/details/json?placeid={place.place_id}&key={googleApiKey}"));
        //            }
        //            List<string> resultList = new List<string>();
        //            var continuation = Task.WhenAll(downloadTasks);
        //            try
        //            {
        //                await continuation;
        //            }
        //            catch (AggregateException)
        //            { }

        //            if (continuation.Status == TaskStatus.RanToCompletion)
        //            {
        //                resultList = continuation.Result.ToList();
        //            }
        //            return resultList;
        //        }
        //        public List<Place> CheckForCachedData(GoogleCity.Example cityObject)
        //        {
        //            var databaseCity = unitOfWork.CityRepository.Get(x => x.GooglePlaceId == cityObject.place_id).FirstOrDefault();
        //            if (databaseCity != null)
        //            {
        //                var databasePlaces = unitOfWork.PlaceRepository.Get(x => x.CityId == databaseCity.Id, includeProperties: "PlaceReviewList").ToList();
        //                return databasePlaces;
        //            }
        //            else
        //            {
        //                GoogleAddress address = new GoogleAddress();
        //                switch (cityObject.address_components.Count)
        //                {
        //                    case 2:
        //                        address.City = cityObject.address_components[0].long_name;
        //                        address.State = cityObject.address_components[1].long_name;
        //                        address.Country = cityObject.address_components[1].long_name;
        //                        break;
        //                    case 3:
        //                        address.City = cityObject.address_components[0].long_name;
        //                        address.State = cityObject.address_components[1].long_name;
        //                        address.Country = cityObject.address_components[2].long_name;
        //                        break;
        //                    case 4:
        //                        address.City = cityObject.address_components[0].long_name;
        //                        address.State = cityObject.address_components[2].long_name;
        //                        address.Country = cityObject.address_components[3].long_name;
        //                        break;
        //                    case 5:
        //                        address.City = cityObject.address_components[0].long_name;
        //                        address.State = cityObject.address_components[3].long_name;
        //                        address.Country = cityObject.address_components[4].long_name;
        //                        break;
        //                }

        //                List<Place> placeList = DownloadPlacesList(cityObject, address);
        //                City city = new City { Name = address.City, PlaceList = placeList, AddedToTable = DateTime.Now, GooglePlaceId = cityObject.place_id };
        //                State state = new State { Name = address.State, CityList = new List<City> { city } };
        //                Country country = new Country { Name = address.Country, StateList = new List<State> { state } };

        //                int countryId = unitOfWork.CountryRepository.Get(x => x.Name.ToLower() == country.Name.ToLower()).Select(x => x.Id).SingleOrDefault();
        //                if (countryId == 0)
        //                {
        //                    unitOfWork.CountryRepository.Insert(country);
        //                }
        //                else
        //                {
        //                    int stateId = unitOfWork.StateRepository.Get(x => x.Name.ToLower() == state.Name.ToLower()).Select(x => x.Id).SingleOrDefault();
        //                    if (stateId == 0)
        //                    {
        //                        state.CountryId = countryId;
        //                        unitOfWork.StateRepository.Insert(state);
        //                    }
        //                    else
        //                    {
        //                        int cityId = unitOfWork.CityRepository.Get(x => x.Name.ToLower() == city.Name.ToLower()).Select(x => x.Id).SingleOrDefault();
        //                        if (cityId == 0)
        //                        {
        //                            city.StateId = stateId;
        //                            unitOfWork.CityRepository.Insert(city);
        //                        }
        //                    }
        //                }
        //                unitOfWork.Save();
        //                return placeList;
        //            }
        //        }
        //        public List<Place> DownloadPlacesList(GoogleCity.Example cityObject, GoogleAddress address)
        //        {
        //            List<Place> placeList = new List<Place>();
        //            string latitude = cityObject.geometry.location.lat.ToString().Replace(",", ".");
        //            string longitude = cityObject.geometry.location.lng.ToString().Replace(",", ".");
        //            string location = $"{latitude},{longitude}";
        //            int radius = 2000;
        //            string keyWord = "museum";
        //            string googleApiKey = ConfigurationManager.AppSettings["googleApiKey"];
        //            string url = $"https://maps.googleapis.com/maps/api/place/nearbysearch/json?location={location}&radius={radius}&type={keyWord}&key={googleApiKey}";
        //            string nextPageToken = null;
        //            do
        //            {
        //                Debug.WriteLine($"Load places list - {DateTime.Now}");

        //                string result = DownloadPage(url);
        //                GooglePlace.Example placeObject = JsonConvert.DeserializeObject<GooglePlace.Example>(result);
        //                var task = Task.Run(async () => await GetResultsAsync(placeObject));
        //                List<string> resultList = task.Result;
        //                foreach (var resultPlace in resultList)
        //                {
        //                    placeList.Add(DownloadPlaceDetails(resultPlace, address));
        //                }
        //                nextPageToken = placeObject.next_page_token;
        //                url = $"https://maps.googleapis.com/maps/api/place/nearbysearch/json?pagetoken={nextPageToken}&key={googleApiKey}";
        //            } while (nextPageToken != null);
        //            return placeList;
        //        }

        //        public string SaveFile(GooglePlaceDetails.Example placeDetails, GoogleAddress address)
        //        {
        //            Debug.WriteLine($"Load photo - {DateTime.Now}");

        //            string filePath = string.Empty;
        //            int maxWidth = 600;
        //            int maxHeight = 600;
        //            string googleApiKey = ConfigurationManager.AppSettings["googleApiKey"];
        //            if (placeDetails.result.photos != null)
        //            {
        //                foreach (var photo in placeDetails.result.photos)
        //                {
        //                    if (photo.height >= maxHeight && photo.width >= maxWidth)
        //                    {
        //                        string photoReference = photo.photo_reference;
        //                        string photoUrl = $"https://maps.googleapis.com/maps/api/place/photo?maxwidth={maxWidth}&maxheight={maxHeight}&photoreference={photoReference}&key={googleApiKey}";
        //                        string fileName = $"{placeDetails.result.place_id}.jpg";
        //                        filePath = $"/Content/Images/Places/{address.Country}/{address.State}/{address.City}/{fileName}";
        //                        string path = HttpContext.Current.Server.MapPath(filePath);
        //                        CheckForDirectoryAndDownloadFile(photoUrl, path);
        //                        break;
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                filePath = "/Content/Images/No-image-available.jpg";
        //            }
        //            return filePath;
        //        }
        //        public string GetPlaceTypes(GooglePlaceDetails.Example placeDetails)
        //        {
        //            string types = string.Empty;
        //            foreach (var type in placeDetails.result.types)
        //            {
        //                types += $"{type}; ";
        //            }
        //            return types;
        //        }
        //        public string GetOpeningHours(GooglePlaceDetails.Example placeDetails)
        //        {
        //            string openingHours = string.Empty;
        //            if (placeDetails.result.opening_hours != null)
        //            {
        //                foreach (var opening in placeDetails.result.opening_hours.weekday_text)
        //                {
        //                    openingHours += $"{opening}; ";
        //                }
        //            }
        //            else
        //            {
        //                openingHours = "No data available!";
        //            }
        //            return openingHours;
        //        }
        //        public string GetInternationalPhoneNumber(GooglePlaceDetails.Example placeDetails)
        //        {
        //            string internationalPhoneNumber = string.Empty;
        //            if (placeDetails.result.international_phone_number != null)
        //            {
        //                internationalPhoneNumber = placeDetails.result.international_phone_number;
        //            }
        //            else
        //            {
        //                internationalPhoneNumber = "No data available!";
        //            }
        //            return internationalPhoneNumber;
        //        }
        //        public void CheckForOutOfDateData()
        //        {
        //            var cityList = unitOfWork.CityRepository.Get();
        //            foreach (City city in cityList)
        //            {
        //                TimeSpan ts = DateTime.Now - city.AddedToTable;
        //                int differenceInDays = ts.Days;
        //                if (differenceInDays >= 30)
        //                {
        //                    unitOfWork.CityRepository.Delete(city);
        //                }
        //            }
        //}
        #endregion
    }
}
