﻿using System.IO;
using System.Web;

namespace LetsTravel.Helpers
{
    public class ServerPathProvider : IPathProvider
    {
        public string MapPath(string path)
        {
            return HttpContext.Current.Server.MapPath(path);
        }
    }

    public class TestPathProvider : IPathProvider
    {
        public string MapPath(string path)
        {
            return Path.Combine("C:/Users/Magnum/Dropbox/Apps/Eleks Learning/TouristProject/TouristProject/", path);
        }
    }
}