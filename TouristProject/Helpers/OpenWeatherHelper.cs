﻿using System.Configuration;
using System.Net;
using System.Text;
using Newtonsoft.Json;
using LetsTravel.Entities.OpenWeatherModels;

namespace LetsTravel.Helpers
{
    public class OpenWeatherHelper : IOpenWeather
    {
        public string GetResult(string url)
        {
            string result = string.Empty;
            using (WebClient client = new WebClient())
            {
                result = Encoding.UTF8.GetString(client.DownloadData(url));
            }
            return result;
        }
        public string GetForecastData(string city)
        {
            string openWeatherApiKey = ConfigurationManager.AppSettings["openWeatherApiKey"];
            string url = $"http://api.openweathermap.org/data/2.5/forecast/daily?q={city}&units=metric&cnt=7&appid={openWeatherApiKey}";
            return GetResult(url);
        }
        public OpenForecast.Example ParseForecastData(string forecastData)
        {
            return JsonConvert.DeserializeObject<OpenForecast.Example>(forecastData);
        }
    }
}