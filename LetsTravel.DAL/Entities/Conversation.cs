﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LetsTravel.Entities
{
    public class Conversation
    {
        public int Id { get; set; }
        public string User1 { get; set; }
        public string User1_Id { get; set; }
        public string User2 { get; set; }
        public string User2_Id { get; set; }
        public DateTime StartedTime { get; set; }
        public virtual List<Message> MessageList { get; set; }
    }
}