﻿using System;

namespace LetsTravel.Entities
{
    public class Message
    {
        public int Id { get; set; }
        public string Sender { get; set; }
        public string MessageText { get; set; }
        public DateTime CreatedTime { get; set; }
        public int ConversationId { get; set; }
        public virtual Conversation Conversation { get; set; }
    }
}