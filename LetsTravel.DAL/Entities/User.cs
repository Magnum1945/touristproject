﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LetsTravel.Entities
{
    public class User
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string Photo { get; set; }
        public string PreferableThings { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime BirthDate { get; set; }
        public bool IsMan { get; set; }
        public string PhoneNumber1 { get; set; }
        public string PhoneNumber2 { get; set; }
        public string Email { get; set; }
        public double Rating { get; set; }
        public virtual List<Language> LanguageList { get; set; }
        public virtual Address Address { get; set; }
        public virtual List<Trip> TripList { get; set; }
    }
}