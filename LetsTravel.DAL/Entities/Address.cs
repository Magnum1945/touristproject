﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LetsTravel.Entities
{
    public class Address
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        [Required(ErrorMessage = "Country is required")]
        public string Country { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public string BuildingNumber { get; set; }
        public string PostalZipCode { get; set; }
        public virtual User User { get; set; }
    }
}