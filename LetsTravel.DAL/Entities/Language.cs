﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LetsTravel.Entities
{
    public class Language
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual List<User> UserList { get; set; }
    }
}