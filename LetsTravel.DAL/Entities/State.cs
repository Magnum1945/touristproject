﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LetsTravel.Entities
{
    public class State
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual List<City> CityList { get; set; }
        public int CountryId { get; set; }
        public virtual Country Country { get; set; }
    }
}