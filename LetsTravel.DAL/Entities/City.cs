﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LetsTravel.Entities
{
    public class City
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual List<Place> PlaceList { get; set; }
        public int StateId { get; set; }
        public virtual State State { get; set; }
    }
}