﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LetsTravel.Entities
{
    public class Post
    {
        public int Id { get; set; }
        public string UserLogin { get; set; }
        public string UserPhoto { get; set; }
        public string PostMessage { get; set; }
        public DateTime DateTime { get; set; }
        public double Rating { get; set; }
        public int TripId { get; set; }
        public virtual Trip Trip { get; set; }
    }
}