﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LetsTravel.Entities
{
    public class Trip
    {
        public int Id { get; set; }
        public string CountryName { get; set; }
        public string StateName { get; set; }
        public string CityName { get; set; }
        public string TripName { get; set; }
        public string UserLogin { get; set; }
        public string Description { get; set; }
        public DateTime StartTime { get; set; }
        public double DurationInSeconds { get; set; }
        public int DistanceInMeters { get; set; }
        public decimal TripPrice { get; set; }
        public string Photo { get; set; }
        public virtual List<Place> PlaceList { get; set; }
        public virtual List<User> UserList { get; set; }
        public virtual List<Post> PostList { get; set; }
    }
}