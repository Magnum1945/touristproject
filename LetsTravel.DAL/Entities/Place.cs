﻿using System.Collections.Generic;

namespace LetsTravel.Entities
{
    public class Place
    {
        public int Id { get; set; }
        public string FormattedAddress { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string InternationalPhoneNumber { get; set; }
        public string Name { get; set; }
        public string OpeningHours { get; set; }
        public string Photo { get; set; }
        public string GooglePlaceId { get; set; }
        public double Rating { get; set; }
        public string Reference { get; set; }
        public string Types { get; set; }
        public double DurationInSeconds { get; set; }
        public string Description { get; set; }
        public string Website { get; set; }
        public int CityId { get; set; }
        public virtual City City { get; set; }
        public virtual List<Trip> TripList { get; set; }
    }
}