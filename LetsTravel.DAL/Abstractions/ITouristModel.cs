﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using LetsTravel.Entities;

namespace LetsTravel.DAL.Abstractions
{
    public interface ITouristModel : IDisposable
    {
        IDbSet<Address> Address { get; set; }
        IDbSet<Place> Place { get; set; }
        IDbSet<Post> Post { get; set; }
        IDbSet<Trip> Trip { get; set; }
        IDbSet<User> User { get; set; }
        IDbSet<Language> Language { get; set; }
        IDbSet<Country> Country { get; set; }
        IDbSet<State> State { get; set; }
        IDbSet<City> City { get; set; }
        IDbSet<Conversation> Conversation { get; set; }
        IDbSet<Message> Message { get; set; }

        void SaveChanges();
        IDbSet<T> Set<T>() where T : class;
        DbEntityEntry Entry<T>(T entity) where T : class;
    }
}
