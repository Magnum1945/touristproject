﻿using System;
using LetsTravel.Entities;

namespace LetsTravel.DAL
{
    public interface IUnitOfWork : IDisposable
    {
        IGenericRepository<User> UserRepository { get; }
        IGenericRepository<Address> AddressRepository { get; }
        IGenericRepository<City> CityRepository { get; }
        IGenericRepository<State> StateRepository { get; }
        IGenericRepository<Country> CountryRepository { get; }
        IGenericRepository<Language> LanguageRepository { get; }
        IGenericRepository<Place> PlaceRepository { get; }
        IGenericRepository<Post> PostRepository { get; }
        IGenericRepository<Trip> TripRepository { get; }
        IGenericRepository<Conversation> ConversationRepository { get; }
        IGenericRepository<Message> MessageRepository { get; }
        void Save();
    }
}
