namespace LetsTravel
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using LetsTravel.DAL.Abstractions;
    using LetsTravel.Entities;

    public class TouristModel : DbContext, ITouristModel
    {
        public TouristModel() : base("name=TouristModel")
        {
            //Strategies for database initialization
            //Database.SetInitializer(new DropCreateDatabaseIfModelChanges<TouristModel>());
            //Database.SetInitializer(new DropCreateDatabaseAlways<TouristModel>());
            //Database.SetInitializer(new CreateDatabaseIfNotExists<TouristModel>());
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //one-to-many relation between Country and State
            modelBuilder.Entity<Country>()
                        .HasKey(s => s.Id)
                        .HasMany<State>(s => s.StateList)
                        .WithRequired(s => s.Country)
                        .HasForeignKey(s => s.CountryId);
            //Set autoincrement to Id column in Country
            modelBuilder.Entity<Country>().Property(s => s.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            //one-to-many relation between State and City
            modelBuilder.Entity<State>()
                        .HasKey(s => s.Id)
                        .HasMany<City>(s => s.CityList)
                        .WithRequired(s => s.State)
                        .HasForeignKey(s => s.StateId);
            //Set autoincrement to Id column in State
            modelBuilder.Entity<State>().Property(s => s.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            //one-to-many relation between City and Place
            modelBuilder.Entity<City>()
                        .HasKey(s => s.Id)
                        .HasMany<Place>(s => s.PlaceList)
                        .WithRequired(s => s.City)
                        .HasForeignKey(s => s.CityId);
            //Set autoincrement to Id column in City
            modelBuilder.Entity<City>().Property(s => s.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            //one-to-one relation between User and Address
            modelBuilder.Entity<User>()
                        .HasRequired(s => s.Address)
                        .WithRequiredPrincipal(s => s.User);
            //Set autoincrement to Id column in User
            modelBuilder.Entity<User>().Property(s => s.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            //many-to-many relation between User and Language
            modelBuilder.Entity<User>()
                .HasKey(s => s.Id)
                .HasMany<Language>(s => s.LanguageList)
                .WithMany(s => s.UserList)
                .Map(s =>
                {
                    s.MapLeftKey("UserId");
                    s.MapRightKey("LanguageId");
                    s.ToTable("UserToLanguage");
                });
            //Set autoincrement to Id column in Language
            modelBuilder.Entity<Language>().Property(s => s.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            //many-to-many relation between Trip and Place
            modelBuilder.Entity<Trip>()
                .HasKey(s => s.Id)
                .HasMany<Place>(s => s.PlaceList)
                .WithMany(s => s.TripList)
                .Map(s =>
                {
                    s.MapLeftKey("TripId");
                    s.MapRightKey("PlaceId");
                    s.ToTable("TripToPlace");
                });

            //many-to-many relation between Trip and User
            modelBuilder.Entity<Trip>()
                .HasKey(s => s.Id)
                .HasMany<User>(s => s.UserList)
                .WithMany(s => s.TripList)
                .Map(s =>
                {
                    s.MapLeftKey("TripId");
                    s.MapRightKey("UserId");
                    s.ToTable("TripToUser");
                });

            //one-to-many relation between Trip and Post
            modelBuilder.Entity<Trip>()
                        .HasKey(s => s.Id)
                        .HasMany<Post>(s => s.PostList)
                        .WithRequired(s => s.Trip)
                        .HasForeignKey(s => s.TripId);

            modelBuilder.Entity<Address>().HasKey(s => s.Id);
            //Set autoincrement to Id column in Place
            modelBuilder.Entity<Place>().Property(s => s.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<Post>().HasKey(s => s.Id);
            //Set autoincrement to Id column in Trip
            modelBuilder.Entity<Post>().Property(s => s.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            //one-to-many relation between Conversation and Message
            modelBuilder.Entity<Conversation>()
                        .HasKey(s => s.Id)
                        .HasMany<Message>(s => s.MessageList)
                        .WithRequired(s => s.Conversation)
                        .HasForeignKey(s => s.ConversationId);
            //Set autoincrement to Id column in Conversation
            modelBuilder.Entity<Conversation>().Property(s => s.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            //Set autoincrement to Id column in Message
            modelBuilder.Entity<Message>().Property(s => s.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
        }

        void ITouristModel.SaveChanges()
        {
            SaveChanges();
        }

        IDbSet<T> ITouristModel.Set<T>()
        {
            return Set<T>();
        }

        DbEntityEntry ITouristModel.Entry<T>(T entity)
        {
            return Entry<T>(entity);
        }

        public IDbSet<Address> Address { get; set; }
        public IDbSet<Place> Place { get; set; }
        public IDbSet<Post> Post { get; set; }
        public IDbSet<Trip> Trip { get; set; }
        public IDbSet<User> User { get; set; }
        public IDbSet<Language> Language { get; set; }
        public IDbSet<Country> Country { get; set; }
        public IDbSet<State> State { get; set; }
        public IDbSet<City> City { get; set; }
        public IDbSet<Conversation> Conversation { get; set; }
        public IDbSet<Message> Message { get; set; }
    }
}