﻿using System;
using System.Data.Entity.Infrastructure;
using System.Linq;
using LetsTravel.DAL.Abstractions;

namespace LetsTravel.DAL
{
    public class UnitOfWork : IDisposable
    {
        private ITouristModel context;
        public UnitOfWork()
        {
            context = new TouristModel();
        }
        public UnitOfWork(ITouristModel context)
        {
            this.context = context;
        }
        public GenericRepository<TEntity> GetRepository<TEntity>() where TEntity : class
        {
            return new GenericRepository<TEntity>(context);
        }
        public void Save()
        {
            bool saveFailed;
            do
            {
                saveFailed = false;
                try
                {
                    context.SaveChanges();
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    saveFailed = true;
                    ex.Entries.Single().Reload();
                }
            } while (saveFailed);
        }
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
