﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using LetsTravel.DAL.Abstractions;

namespace LetsTravel.DAL
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class
    {
        private ITouristModel context;
        public GenericRepository(ITouristModel context)
        {
            this.context = context;
        }
        public virtual IQueryable<TEntity> Get(Expression<Func<TEntity, bool>> filter = null,
                                               Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
                                               string includeProperties = "")
        {
            //IQueryable<TEntity> query = dbSet;
            IQueryable<TEntity> query = context.Set<TEntity>().AsQueryable();
            if (filter != null)
            {
                query = query.Where(filter);
            }
            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }
            if (orderBy != null)
            {
                return orderBy(query).ToList().AsQueryable();
            }
            else
            {
                return query.ToList().AsQueryable();
            }
        }
        public virtual TEntity GetByID(object id)
        {
            return context.Set<TEntity>().Find(id);
        }
        public virtual void Insert(TEntity entity)
        {
            context.Set<TEntity>().Add(entity);
        }
        public virtual void Delete(object id)
        {
            TEntity entityToDelete = context.Set<TEntity>().Find(id);
            Delete(entityToDelete);
        }
        public virtual void Delete(TEntity entityToDelete)
        {
            //if (context.Entry(entityToDelete).State == EntityState.Detached)
            //{
            //    context.Set<TEntity>().Attach(entityToDelete);
            //}
            context.Set<TEntity>().Remove(entityToDelete);
        }
        public virtual void Update(TEntity entityToUpdate)
        {
            context.Set<TEntity>().Attach(entityToUpdate);
            context.Entry(entityToUpdate).State = EntityState.Modified;
        }
        public virtual void Update(TEntity entityFromDatabase, TEntity entityToUpdate)
        {
            context.Entry(entityFromDatabase).CurrentValues.SetValues(entityToUpdate);
        }
    }
}