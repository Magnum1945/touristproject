﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.OData;
using System.Web.Http.Results;
using LetsTravel.Controllers.WebApi;
using LetsTravel.DAL.Abstractions;
using LetsTravel.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace LetsTravel.Tests
{
    [TestClass]
    public class TestUserController
    {
        Mock<ITouristModel> mock;
        UserController userController;
        public TestUserController()
        {
            this.mock = new Mock<ITouristModel>();
            this.userController = new UserController(mock.Object);
        }

        public void SetupMock()
        {
            mock.Setup(x => x.Set<User>()).Returns(new TestDbSet<User>
                {
                    new User {Id = 1, Email = "test@test.com"}, new User {Id = 2, Email = "test2@test.com"}
                });
        }

        [TestMethod]
        public void TestGetAllUsers()
        {
            //Arrange
            SetupMock();

            // Act
            var allUsers = userController.GetUser();

            // Assert
            Assert.AreEqual(2, allUsers.Count());
        }

        [TestMethod]
        public void TestGetUsersWithId()
        {
            //Arrange
            SetupMock();

            // Act
            var databaseUser = userController.GetUser(1);
            var user = new User { Id = 1 };

            // Assert
            Assert.AreEqual(user.Id, databaseUser.Queryable.SingleOrDefault().Id);
        }

        [TestMethod]
        public void TestPutNullUser()
        {
            //Arrange
            SetupMock();

            HttpConfiguration configuration = new HttpConfiguration();
            HttpRequestMessage request = new HttpRequestMessage();
            userController.Request = request;
            userController.Request.Properties["MS_HttpConfiguration"] = configuration;

            // Act
            Delta<User> delta = new Delta<User>();
            delta.TrySetPropertyValue("FirstName", "Johny");
            delta.TrySetPropertyValue("LastName", "Johanson");
            IHttpActionResult result = userController.Put(10, delta);
            var contentResult = result as NotFoundResult;

            // Assert
            Assert.IsInstanceOfType(contentResult, typeof(NotFoundResult));
        }

        [TestMethod]
        public void TestPostUser()
        {
            //Arrange
            SetupMock();

            // Act
            var user = new User { Id = 3, Email = "test3@test.com" };
            IHttpActionResult result = userController.Post(user);
            var contentResult = result as StatusCodeResult;

            // Assert
            Assert.AreEqual(HttpStatusCode.NoContent, contentResult.StatusCode);
        }

        [TestMethod]
        public void TestPostSameUser()
        {
            //Arrange
            SetupMock();

            // Act
            var user = new User { Id = 3, Email = "test@test.com" };
            IHttpActionResult result = userController.Post(user);
            var contentResult = result as BadRequestErrorMessageResult;

            // Assert
            Assert.AreEqual("User already in the database", contentResult.Message);
            Assert.IsInstanceOfType(result, typeof(BadRequestErrorMessageResult));
        }

        [TestMethod]
        public void TestDeleteUser()
        {
            //Arrange
            SetupMock();

            // Act
            var result = userController.Delete(1);
            var contentResult = result as StatusCodeResult;

            // Assert
            Assert.AreEqual(HttpStatusCode.NoContent, contentResult.StatusCode);
        }
    }
}
