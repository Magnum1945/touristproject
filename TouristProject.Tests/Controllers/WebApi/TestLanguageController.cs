﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.OData;
using System.Web.Http.Results;
using LetsTravel.Controllers.WebApi;
using LetsTravel.DAL.Abstractions;
using LetsTravel.Entities;
using LetsTravel.Tests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace TouristProject.Tests.Controllers.WebApi
{
    [TestClass]
    public class TestLanguageController
    {
        Mock<ITouristModel> mock;
        LanguageController languageController;
        public TestLanguageController()
        {
            this.mock = new Mock<ITouristModel>();
            this.languageController = new LanguageController(mock.Object);
        }

        public void SetupMock()
        {
            mock.Setup(x => x.Set<Language>()).Returns(new TestDbSet<Language>
                {
                    new Language {Id = 1, Name = "English"}, new Language {Id = 2, Name = "Spanish"}
                });
        }

        [TestMethod]
        public void TestGetAllLanguages()
        {
            //Arrange
            SetupMock();

            // Act
            var allLanguages = languageController.GetLanguage();

            // Assert
            Assert.AreEqual(2, allLanguages.Count());
        }

        [TestMethod]
        public void TestGetLanguageWithId()
        {
            //Arrange
            SetupMock();

            // Act
            var databaseLanguage = languageController.GetLanguage(1);
            var language = new Language { Id = 1 };

            // Assert
            Assert.AreEqual(language.Id, databaseLanguage.Queryable.SingleOrDefault().Id);
        }
        [TestMethod]
        public void TestPostLanguage()
        {
            //Arrange
            SetupMock();

            // Act
            var language = new Language { Id = 3, Name = "Italian" };
            IHttpActionResult result = languageController.Post(language);
            var contentResult = result as StatusCodeResult;

            // Assert
            Assert.AreEqual(HttpStatusCode.NoContent, contentResult.StatusCode);
        }
        [TestMethod]
        public void TestPostSameLanguage()
        {
            //Arrange
            SetupMock();

            // Act
            var language = new Language { Id = 3, Name = "English" };
            IHttpActionResult result = languageController.Post(language);
            var contentResult = result as BadRequestErrorMessageResult;

            // Assert
            Assert.AreEqual("Language already in the database", contentResult.Message);
            Assert.IsInstanceOfType(result, typeof(BadRequestErrorMessageResult));
        }

        [TestMethod]
        public void TestDeleteLanguage()
        {
            //Arrange
            SetupMock();

            // Act
            var result = languageController.Delete(1);
            var contentResult = result as StatusCodeResult;

            // Assert
            Assert.AreEqual(HttpStatusCode.NoContent, contentResult.StatusCode);
        }

        [TestMethod]
        public void TestPutNullLanguage()
        {
            //Arrange
            SetupMock();

            HttpConfiguration configuration = new HttpConfiguration();
            HttpRequestMessage request = new HttpRequestMessage();
            languageController.Request = request;
            languageController.Request.Properties["MS_HttpConfiguration"] = configuration;

            // Act
            Delta<Language> delta = new Delta<Language>();
            delta.TrySetPropertyValue("Name", "Test language");
            IHttpActionResult result = languageController.Put(10, delta);
            var contentResult = result as NotFoundResult;

            // Assert
            Assert.IsInstanceOfType(contentResult, typeof(NotFoundResult));
        }
    }
}
