﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.OData;
using System.Web.Http.Results;
using LetsTravel.Controllers.WebApi;
using LetsTravel.DAL.Abstractions;
using LetsTravel.Entities;
using LetsTravel.Tests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace TouristProject.Tests.Controllers.WebApi
{
    [TestClass]
    public class TestCityController
    {
        Mock<ITouristModel> mock;
        CityController cityController;
        public TestCityController()
        {
            this.mock = new Mock<ITouristModel>();
            this.cityController = new CityController(mock.Object);
        }

        public void SetupMock()
        {
            mock.Setup(x => x.Set<City>()).Returns(new TestDbSet<City>
                {
                    new City {Id = 1, Name = "Lviv"}, new City {Id = 2, Name = "Kyiv"}
                });
        }

        [TestMethod]
        public void TestGetAllCities()
        {
            //Arrange
            SetupMock();

            // Act
            var allCities = cityController.GetCity();

            // Assert
            Assert.AreEqual(2, allCities.Count());
        }

        [TestMethod]
        public void TestGetCityWithId()
        {
            //Arrange
            SetupMock();

            // Act
            var databaseCity = cityController.GetCity(1);
            var city = new City { Id = 1 };

            // Assert
            Assert.AreEqual(city.Id, databaseCity.Queryable.SingleOrDefault().Id);
        }

        [TestMethod]
        public void TestPostCity()
        {
            //Arrange
            SetupMock();

            // Act
            var city = new City { Id = 3, Name = "Berlin" };
            IHttpActionResult result = cityController.Post(city);
            var contentResult = result as StatusCodeResult;

            // Assert
            Assert.AreEqual(HttpStatusCode.NoContent, contentResult.StatusCode);
        }

        [TestMethod]
        public void TestPostSameCity()
        {
            //Arrange
            SetupMock();

            // Act
            var city = new City { Id = 3, Name = "Lviv" };
            IHttpActionResult result = cityController.Post(city);
            var contentResult = result as BadRequestErrorMessageResult;

            // Assert
            Assert.AreEqual("City already in the database", contentResult.Message);
            Assert.IsInstanceOfType(result, typeof(BadRequestErrorMessageResult));
        }

        [TestMethod]
        public void TestDeleteCity()
        {
            //Arrange
            SetupMock();

            // Act
            var result = cityController.Delete(1);
            var contentResult = result as StatusCodeResult;

            // Assert
            Assert.AreEqual(HttpStatusCode.NoContent, contentResult.StatusCode);
        }

        [TestMethod]
        public void TestPutNullCity()
        {
            //Arrange
            SetupMock();

            HttpConfiguration configuration = new HttpConfiguration();
            HttpRequestMessage request = new HttpRequestMessage();
            cityController.Request = request;
            cityController.Request.Properties["MS_HttpConfiguration"] = configuration;

            // Act
            Delta<City> delta = new Delta<City>();
            delta.TrySetPropertyValue("Name", "Test city");
            IHttpActionResult result = cityController.Put(10, delta);
            var contentResult = result as NotFoundResult;

            // Assert
            Assert.IsInstanceOfType(contentResult, typeof(NotFoundResult));
        }
    }
}
