﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.OData;
using System.Web.Http.Results;
using LetsTravel.Controllers.WebApi;
using LetsTravel.DAL.Abstractions;
using LetsTravel.Entities;
using LetsTravel.Tests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace TouristProject.Tests.Controllers.WebApi
{
    [TestClass]
    public class TestAddressController
    {
        Mock<ITouristModel> mock;
        AddressController addressController;
        public TestAddressController()
        {
            this.mock = new Mock<ITouristModel>();
            this.addressController = new AddressController(mock.Object);
        }

        public void SetupMock()
        {
            mock.Setup(x => x.Set<Address>()).Returns(new TestDbSet<Address>
                {
                    new Address {Id = 1}, new Address {Id = 2}
                });
        }

        [TestMethod]
        public void TestGetAllAddress()
        {
            //Arrange
            SetupMock();

            // Act
            var allAddresses = addressController.GetAddress();

            // Assert
            Assert.AreEqual(2, allAddresses.Count());
        }

        [TestMethod]
        public void TestGetAddressWithId()
        {
            //Arrange
            SetupMock();

            // Act
            var databaseAddress = addressController.GetAddress(1);
            var address = new Address { Id = 1 };

            // Assert
            Assert.AreEqual(address.Id, databaseAddress.Queryable.SingleOrDefault().Id);
        }

        [TestMethod]
        public void TestPostAddress()
        {
            //Arrange
            SetupMock();

            // Act
            var address = new Address { Id = 3 };
            IHttpActionResult result = addressController.Post(address);
            var contentResult = result as StatusCodeResult;

            // Assert
            Assert.AreEqual(HttpStatusCode.NoContent, contentResult.StatusCode);
        }

        [TestMethod]
        public void TestDeleteAddress()
        {
            //Arrange
            SetupMock();

            // Act
            var result = addressController.Delete(1);
            var contentResult = result as StatusCodeResult;

            // Assert
            Assert.AreEqual(HttpStatusCode.NoContent, contentResult.StatusCode);
        }

        [TestMethod]
        public void TestPutNullAddress()
        {
            //Arrange
            SetupMock();

            HttpConfiguration configuration = new HttpConfiguration();
            HttpRequestMessage request = new HttpRequestMessage();
            addressController.Request = request;
            addressController.Request.Properties["MS_HttpConfiguration"] = configuration;

            // Act
            Delta<Address> delta = new Delta<Address>();
            delta.TrySetPropertyValue("Id", 3);
            delta.TrySetPropertyValue("Country", "Test country");
            IHttpActionResult result = addressController.Put(10, delta);
            var contentResult = result as NotFoundResult;

            // Assert
            Assert.IsInstanceOfType(contentResult, typeof(NotFoundResult));
        }
    }
}
