﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LetsTravel.Controllers.WebApi;
using LetsTravel.DAL.Abstractions;
using LetsTravel.Entities;
using LetsTravel.Helpers;
using LetsTravel.Tests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace TouristProject.Tests.Controllers.WebApi
{
    [TestClass]
    public class TestPhotoController
    {
        Mock<ITouristModel> mock;
        IPathProvider pathProvider;
        PhotoController photoController;
        public TestPhotoController()
        {
            this.mock = new Mock<ITouristModel>();
            this.pathProvider = new TestPathProvider();
            this.photoController = new PhotoController(mock.Object, pathProvider);
        }
        public void SetupMock()
        {
            mock.Setup(x => x.Set<Country>()).Returns(new TestDbSet<Country>
                {
                    new Country {Id = 1, Name = "USA"}, new Country {Id = 2, Name = "Germany"}
                });
            mock.Setup(x => x.Set<State>()).Returns(new TestDbSet<State>
                {
                    new State { Id = 1, Name = "Visconsin" }, new State { Id = 1, Name = "Prussia" }
                });
            mock.Setup(x => x.Set<City>()).Returns(new TestDbSet<City>
                {
                    new City { Id = 1, Name = "Washington" }, new City { Id = 1, Name = "Berlin" }
                });
            mock.Setup(x => x.Set<Place>()).Returns(new TestDbSet<Place>
                {
                    new Place { Id = 2, GooglePlaceId = "123", CityId = 2, Photo = "Content/Images/ukraine/lviv oblast/l'viv/ChIJiWq8-XLdOkcRH8_-tOw_ePk.jpg" },
                    new Place { Id = 1, GooglePlaceId = "XYZ", CityId = 1 }
                });
            mock.Setup(x => x.Set<Trip>()).Returns(new TestDbSet<Trip>
                {
                    new Trip {Id = 1, UserLogin = "test1@test.com", Photo = "Content/Images/ukraine/lviv oblast/l'viv/ChIJiWq8-XLdOkcRH8_-tOw_ePk.jpg"},
                    new Trip {Id = 2, UserLogin = "test2@test.com"}
                });
            mock.Setup(x => x.Set<User>()).Returns(new TestDbSet<User>
                {
                    new User {Id = 1, Email = "test@test.com", Photo = "Content/Images/ukraine/lviv oblast/l'viv/ChIJIevgrm3dOkcRwkJXsJu3ppg.jpg"},
                    new User {Id = 2, Email = "test2@test.com"}
                });
        }

        [TestMethod]
        public void TestGetAllCityPhotoByCityId()
        {
            //Arrange
            SetupMock();

            // Act
            HttpResponseMessage databasePhoto = photoController.Get(2);

            // Assert
            Assert.AreEqual(HttpStatusCode.OK, databasePhoto.StatusCode);
        }

        [TestMethod]
        public void TestGetAllCityPhotoNotFound()
        {
            //Arrange
            SetupMock();

            // Act
            HttpResponseMessage databasePhoto = photoController.Get(3);

            // Assert
            Assert.AreEqual(HttpStatusCode.NotFound, databasePhoto.StatusCode);
        }

        [TestMethod]
        public void TestGetAllCityPhotoInternalError()
        {
            //Arrange
            SetupMock();

            // Act
            HttpResponseMessage databasePhoto = photoController.Get(1);

            // Assert
            Assert.AreEqual(HttpStatusCode.InternalServerError, databasePhoto.StatusCode);
        }

        [TestMethod]
        public void TestGetPlacePhotoNotFound()
        {
            //Arrange
            SetupMock();

            // Act
            HttpResponseMessage databasePhoto = photoController.GetPlacePhoto(10);

            // Assert
            Assert.AreEqual(HttpStatusCode.NotFound, databasePhoto.StatusCode);
        }

        [TestMethod]
        public void TestGetPlacePhotoOk()
        {
            //Arrange
            SetupMock();

            // Act
            HttpResponseMessage databasePhoto = photoController.GetPlacePhoto(2);

            // Assert
            Assert.AreEqual(HttpStatusCode.OK, databasePhoto.StatusCode);
        }

        [TestMethod]
        public void TestGetUserPhotoNotFound()
        {
            //Arrange
            SetupMock();

            // Act
            HttpResponseMessage databasePhoto = photoController.GetUserPhoto(10);

            // Assert
            Assert.AreEqual(HttpStatusCode.NotFound, databasePhoto.StatusCode);
        }

        [TestMethod]
        public void TestGetUserPhotoOk()
        {
            //Arrange
            SetupMock();

            // Act
            HttpResponseMessage databasePhoto = photoController.GetUserPhoto(1);

            // Assert
            Assert.AreEqual(HttpStatusCode.OK, databasePhoto.StatusCode);
        }

        [TestMethod]
        public void TestGetTripPhotoNotFound()
        {
            //Arrange
            SetupMock();

            // Act
            HttpResponseMessage databasePhoto = photoController.GetTripPhoto(10);

            // Assert
            Assert.AreEqual(HttpStatusCode.NotFound, databasePhoto.StatusCode);
        }

        [TestMethod]
        public void TestGetTripPhotoOk()
        {
            //Arrange
            SetupMock();

            // Act
            HttpResponseMessage databasePhoto = photoController.GetTripPhoto(1);

            // Assert
            Assert.AreEqual(HttpStatusCode.OK, databasePhoto.StatusCode);
        }

        [TestMethod]
        public void TestPostPlacePhoto()
        {
            //Arrange
            SetupMock();

            // Act
            HttpResponseMessage result = photoController.PostPlacePhoto(1);

            // Assert
            Assert.AreEqual(HttpStatusCode.NoContent, result.StatusCode);
        }

        [TestMethod]
        public void TestDeleteNullPlacePhoto()
        {
            //Arrange
            SetupMock();

            // Act
            HttpResponseMessage databasePhoto = photoController.DeletePlacePhoto(10);

            // Assert
            Assert.AreEqual(HttpStatusCode.NotFound, databasePhoto.StatusCode);
        }

        [TestMethod]
        public void TestDeleteNullUserPhoto()
        {
            //Arrange
            SetupMock();

            // Act
            HttpResponseMessage databasePhoto = photoController.DeleteUserPhoto(10);

            // Assert
            Assert.AreEqual(HttpStatusCode.NotFound, databasePhoto.StatusCode);
        }

        [TestMethod]
        public void TestDeleteNullTripPhoto()
        {
            //Arrange
            SetupMock();

            // Act
            HttpResponseMessage databasePhoto = photoController.DeleteTripPhoto(10);

            // Assert
            Assert.AreEqual(HttpStatusCode.NotFound, databasePhoto.StatusCode);
        }

        [TestMethod]
        public void TestDeletePhotoNotFound()
        {
            //Arrange
            SetupMock();

            // Act
            HttpResponseMessage databasePhoto = photoController.DeletePhoto("");

            // Assert
            Assert.AreEqual(HttpStatusCode.NoContent, databasePhoto.StatusCode);
        }

        //[TestMethod]
        //public void TestDeletePhotoOK()
        //{
        //    //Arrange
        //    SetupMock();

        //    // Act
        //    HttpResponseMessage databasePhoto = photoController.DeletePhoto("Content/Images/ukraine/lviv oblast/l'viv/ChIJW5HqcG3dOkcRr1saoCFK4wM.jpg");

        //    // Assert
        //    Assert.AreEqual(HttpStatusCode.OK, databasePhoto.StatusCode);
        //}
    }
}
