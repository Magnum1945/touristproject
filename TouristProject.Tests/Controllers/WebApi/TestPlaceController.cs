﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.OData;
using System.Web.Http.Results;
using LetsTravel.Controllers.WebApi;
using LetsTravel.DAL.Abstractions;
using LetsTravel.Entities;
using LetsTravel.Tests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace TouristProject.Tests.Controllers.WebApi
{
    [TestClass]
    public class TestPlaceController
    {
        Mock<ITouristModel> mock;
        PlaceController placeController;
        public TestPlaceController()
        {
            this.mock = new Mock<ITouristModel>();
            this.placeController = new PlaceController(mock.Object);
        }

        public void SetupMock()
        {
            mock.Setup(x => x.Set<Place>()).Returns(new TestDbSet<Place>
                {
                    new Place {Id = 1, GooglePlaceId = "XYZ"},
                    new Place {Id = 2, GooglePlaceId = "123"}
                });
        }

        [TestMethod]
        public void TestGetAllPlaces()
        {
            //Arrange
            SetupMock();

            // Act
            var allPlaces = placeController.GetPlace();

            // Assert
            Assert.AreEqual(2, allPlaces.Count());
        }

        [TestMethod]
        public void TestGetPlaceWithId()
        {
            //Arrange
            SetupMock();

            // Act
            var databasePlace = placeController.GetPlace("123");
            var place = new Place { Id = 1, GooglePlaceId = "123" };

            // Assert
            Assert.AreEqual(place.GooglePlaceId, databasePlace.Queryable.SingleOrDefault().GooglePlaceId);
        }
        [TestMethod]
        public void TestPostPlace()
        {
            //Arrange
            SetupMock();

            // Act
            var place = new Place { Id = 3, GooglePlaceId = "XYZ123" };
            IHttpActionResult result = placeController.Post(place);
            var contentResult = result as StatusCodeResult;

            // Assert
            Assert.AreEqual(HttpStatusCode.NoContent, contentResult.StatusCode);
        }
        [TestMethod]
        public void TestPostSamePlace()
        {
            //Arrange
            SetupMock();

            // Act
            var place = new Place { Id = 3, GooglePlaceId = "123" };
            IHttpActionResult result = placeController.Post(place);
            var contentResult = result as BadRequestErrorMessageResult;

            // Assert
            Assert.AreEqual("Place already in the database", contentResult.Message);
            Assert.IsInstanceOfType(result, typeof(BadRequestErrorMessageResult));
        }

        [TestMethod]
        public void TestDeletePlace()
        {
            //Arrange
            SetupMock();

            // Act
            var result = placeController.Delete(1);
            var contentResult = result as StatusCodeResult;

            // Assert
            Assert.AreEqual(HttpStatusCode.NoContent, contentResult.StatusCode);
        }

        [TestMethod]
        public void TestPutNullPlace()
        {
            //Arrange
            SetupMock();

            HttpConfiguration configuration = new HttpConfiguration();
            HttpRequestMessage request = new HttpRequestMessage();
            placeController.Request = request;
            placeController.Request.Properties["MS_HttpConfiguration"] = configuration;

            // Act
            Delta<Place> delta = new Delta<Place>();
            delta.TrySetPropertyValue("Name", "Test place");
            IHttpActionResult result = placeController.Put(10, delta);
            var contentResult = result as NotFoundResult;

            // Assert
            Assert.IsInstanceOfType(contentResult, typeof(NotFoundResult));
        }
    }
}
