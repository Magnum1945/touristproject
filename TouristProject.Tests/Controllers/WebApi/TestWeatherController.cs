﻿using System;
using LetsTravel.Controllers.WebApi;
using LetsTravel.DAL.Abstractions;
using LetsTravel.Entities.OpenWeatherModels;
using LetsTravel.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace TouristProject.Tests.Controllers.WebApi
{
    [TestClass]
    public class TestWeatherController
    {
        Mock<IOpenWeather> mock;
        WeatherController weatherController;
        public TestWeatherController()
        {
            this.mock = new Mock<IOpenWeather>();
            this.weatherController = new WeatherController(mock.Object);
        }

        public void SetupMock()
        {
            mock.Setup(x => x.GetResult(It.IsAny<string>())).Returns(() => "HTML SERVER RESPONSE");
            mock.Setup(x => x.GetForecastData("test")).Returns("test");
            mock.Setup(x => x.ParseForecastData("test")).Returns(new OpenForecast.Example());
        }

        [TestMethod]
        public void TestGetWeather()
        {
            //Arrange
            SetupMock();

            // Act
            var forecast = weatherController.Get("test");

            // Assert
            Assert.IsInstanceOfType(forecast, typeof(OpenForecast.Example));
        }
    }
}
