﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.OData;
using System.Web.Http.Results;
using LetsTravel.Controllers.WebApi;
using LetsTravel.DAL.Abstractions;
using LetsTravel.Entities;
using LetsTravel.Tests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace TouristProject.Tests.Controllers.WebApi
{
    [TestClass]
    public class TestPostController
    {
        Mock<ITouristModel> mock;
        PostController postController;
        public TestPostController()
        {
            this.mock = new Mock<ITouristModel>();
            this.postController = new PostController(mock.Object);
        }

        public void SetupMock()
        {
            mock.Setup(x => x.Set<Post>()).Returns(new TestDbSet<Post>
                {
                    new Post {Id = 1, UserLogin = "test1@test.com", PostMessage = "Test1"},
                    new Post {Id = 2, UserLogin = "test2@test.com", PostMessage = "Test2"}
                });
            mock.Setup(x => x.Set<User>()).Returns(new TestDbSet<User>
                {
                    new User {Id = 1, Login = "test1@test.com"}
                });
        }

        [TestMethod]
        public void TestGetAllPosts()
        {
            //Arrange
            SetupMock();

            // Act
            var allPosts = postController.GetPost();

            // Assert
            Assert.AreEqual(2, allPosts.Count());
        }

        [TestMethod]
        public void TestGetPostWithId()
        {
            //Arrange
            SetupMock();

            // Act
            var databasePost = postController.GetPost(1);
            var post = new Post { Id = 1 };

            // Assert
            Assert.AreEqual(post.Id, databasePost.Queryable.SingleOrDefault().Id);
        }
        [TestMethod]
        public void TestPostPost()
        {
            //Arrange
            SetupMock();

            // Act
            var post = new Post { Id = 3, UserLogin = "test1@test.com", PostMessage = "Test3" };
            IHttpActionResult result = postController.Post(post);
            var contentResult = result as StatusCodeResult;

            // Assert
            Assert.AreEqual(HttpStatusCode.NoContent, contentResult.StatusCode);
        }
        [TestMethod]
        public void TestPostSamePost()
        {
            //Arrange
            SetupMock();

            // Act
            var post = new Post { Id = 3, UserLogin = "test2@test.com", PostMessage = "Test2" };
            IHttpActionResult result = postController.Post(post);
            var contentResult = result as BadRequestErrorMessageResult;

            // Assert
            Assert.AreEqual("Wrong user", contentResult.Message);
            Assert.IsInstanceOfType(result, typeof(BadRequestErrorMessageResult));
        }

        [TestMethod]
        public void TestDeletePost()
        {
            //Arrange
            SetupMock();

            // Act
            var result = postController.Delete(1);
            var contentResult = result as StatusCodeResult;

            // Assert
            Assert.AreEqual(HttpStatusCode.NoContent, contentResult.StatusCode);
        }

        [TestMethod]
        public void TestPutNullPost()
        {
            //Arrange
            SetupMock();

            HttpConfiguration configuration = new HttpConfiguration();
            HttpRequestMessage request = new HttpRequestMessage();
            postController.Request = request;
            postController.Request.Properties["MS_HttpConfiguration"] = configuration;

            // Act
            Delta<Post> delta = new Delta<Post>();
            delta.TrySetPropertyValue("UserLogin", "Test login");
            IHttpActionResult result = postController.Put(10, delta);
            var contentResult = result as NotFoundResult;

            // Assert
            Assert.IsInstanceOfType(contentResult, typeof(NotFoundResult));
        }
    }
}
