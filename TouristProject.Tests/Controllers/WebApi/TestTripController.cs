﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.OData;
using System.Web.Http.Results;
using LetsTravel.Controllers.WebApi;
using LetsTravel.DAL.Abstractions;
using LetsTravel.Entities;
using LetsTravel.Tests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace TouristProject.Tests.Controllers.WebApi
{
    [TestClass]
    public class TestTripController
    {
        Mock<ITouristModel> mock;
        TripController tripController;
        public TestTripController()
        {
            this.mock = new Mock<ITouristModel>();
            this.tripController = new TripController(mock.Object);
        }

        public void SetupMock()
        {
            mock.Setup(x => x.Set<Trip>()).Returns(new TestDbSet<Trip>
                {
                    new Trip {Id = 1, UserLogin = "test1@test.com"},
                    new Trip {Id = 2, UserLogin = "test2@test.com"}
                });
            mock.Setup(x => x.Set<User>()).Returns(new TestDbSet<User>
                {
                    new User {Id = 1, Login = "test1@test.com"}
                });
        }

        [TestMethod]
        public void TestGetAllTrips()
        {
            //Arrange
            SetupMock();

            // Act
            var allTrips = tripController.GetTrip();

            // Assert
            Assert.AreEqual(2, allTrips.Count());
        }

        [TestMethod]
        public void TestGetTripWithId()
        {
            //Arrange
            SetupMock();

            // Act
            var databaseTrip = tripController.GetTrip(1);
            var trip = new Trip { Id = 1 };

            // Assert
            Assert.AreEqual(trip.Id, databaseTrip.Queryable.SingleOrDefault().Id);
        }
        [TestMethod]
        public void TestPostTrip()
        {
            //Arrange
            SetupMock();

            // Act
            var trip = new Trip { Id = 3, UserLogin = "test1@test.com" };
            IHttpActionResult result = tripController.Post(trip);
            var contentResult = result as StatusCodeResult;

            // Assert
            Assert.AreEqual(HttpStatusCode.NoContent, contentResult.StatusCode);
        }
        [TestMethod]
        public void TestPostSameTrip()
        {
            //Arrange
            SetupMock();

            // Act
            var trip = new Trip { Id = 3, UserLogin = "test3@test.com" };
            IHttpActionResult result = tripController.Post(trip);
            var contentResult = result as BadRequestErrorMessageResult;

            // Assert
            Assert.AreEqual("Wrong user", contentResult.Message);
            Assert.IsInstanceOfType(result, typeof(BadRequestErrorMessageResult));
        }

        [TestMethod]
        public void TestDeleteTrip()
        {
            //Arrange
            SetupMock();

            // Act
            var result = tripController.Delete(1);
            var contentResult = result as StatusCodeResult;

            // Assert
            Assert.AreEqual(HttpStatusCode.NoContent, contentResult.StatusCode);
        }

        [TestMethod]
        public void TestPutNullTrip()
        {
            //Arrange
            SetupMock();

            HttpConfiguration configuration = new HttpConfiguration();
            HttpRequestMessage request = new HttpRequestMessage();
            tripController.Request = request;
            tripController.Request.Properties["MS_HttpConfiguration"] = configuration;

            // Act
            Delta<Trip> delta = new Delta<Trip>();
            delta.TrySetPropertyValue("UserLogin", "Test login");
            IHttpActionResult result = tripController.Put(10, delta);
            var contentResult = result as NotFoundResult;

            // Assert
            Assert.IsInstanceOfType(contentResult, typeof(NotFoundResult));
        }
    }
}
