﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.OData;
using System.Web.Http.Results;
using LetsTravel.Controllers.WebApi;
using LetsTravel.DAL.Abstractions;
using LetsTravel.Entities;
using LetsTravel.Tests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace TouristProject.Tests.Controllers.WebApi
{
    [TestClass]
    public class TestConversationController
    {
        Mock<ITouristModel> mock;
        ConversationController conversationController;
        public TestConversationController()
        {
            this.mock = new Mock<ITouristModel>();
            this.conversationController = new ConversationController(mock.Object);
        }

        public void SetupMock()
        {
            mock.Setup(x => x.Set<Conversation>()).Returns(new TestDbSet<Conversation>
                {
                    new Conversation {Id = 1, User1 = "user1@test.com", User2= "user2@test.com"},
                    new Conversation {Id = 2, User1 = "user3@test.com", User2= "user4@test.com"}
                });
        }

        [TestMethod]
        public void TestGetAllConversations()
        {
            //Arrange
            SetupMock();

            // Act
            var allConversations = conversationController.GetConversation();

            // Assert
            Assert.AreEqual(2, allConversations.Count());
        }

        [TestMethod]
        public void TestGetConversationWithId()
        {
            //Arrange
            SetupMock();

            // Act
            var databaseConversation = conversationController.GetConversation(1);
            var conversation = new Conversation { Id = 1 };

            // Assert
            Assert.AreEqual(conversation.Id, databaseConversation.Queryable.SingleOrDefault().Id);
        }

        [TestMethod]
        public void TestPostConversation()
        {
            //Arrange
            SetupMock();

            // Act
            var conversation = new Conversation { Id = 3, User1 = "user5@test.com", User2 = "user6@test.com" };
            IHttpActionResult result = conversationController.Post(conversation);
            var contentResult = result as StatusCodeResult;

            // Assert
            Assert.AreEqual(HttpStatusCode.NoContent, contentResult.StatusCode);
        }

        [TestMethod]
        public void TestPostSameConversation()
        {
            //Arrange
            SetupMock();

            // Act
            var conversation = new Conversation { Id = 3, User1 = "user3@test.com", User2 = "user4@test.com" };
            IHttpActionResult result = conversationController.Post(conversation);
            var contentResult = result as BadRequestErrorMessageResult;

            // Assert
            Assert.AreEqual("Conversation already in the database", contentResult.Message);
            Assert.IsInstanceOfType(result, typeof(BadRequestErrorMessageResult));
        }

        [TestMethod]
        public void TestDeleteConversation()
        {
            //Arrange
            SetupMock();

            // Act
            var result = conversationController.Delete(1);
            var contentResult = result as StatusCodeResult;

            // Assert
            Assert.AreEqual(HttpStatusCode.NoContent, contentResult.StatusCode);
        }

        [TestMethod]
        public void TestPutNullConversation()
        {
            //Arrange
            SetupMock();

            HttpConfiguration configuration = new HttpConfiguration();
            HttpRequestMessage request = new HttpRequestMessage();
            conversationController.Request = request;
            conversationController.Request.Properties["MS_HttpConfiguration"] = configuration;

            // Act
            Delta<Conversation> delta = new Delta<Conversation>();
            delta.TrySetPropertyValue("User1", "test user");
            IHttpActionResult result = conversationController.Put(10, delta);
            var contentResult = result as NotFoundResult;

            // Assert
            Assert.IsInstanceOfType(contentResult, typeof(NotFoundResult));
        }
    }
}
