﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.OData;
using System.Web.Http.Results;
using LetsTravel.Controllers.WebApi;
using LetsTravel.DAL.Abstractions;
using LetsTravel.Entities;
using LetsTravel.Tests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;


namespace TouristProject.Tests.Controllers.WebApi
{
    [TestClass]
    public class TestCountryController
    {
        Mock<ITouristModel> mock;
        CountryController countryController;
        public TestCountryController()
        {
            this.mock = new Mock<ITouristModel>();
            this.countryController = new CountryController(mock.Object);
        }
        public void SetupMock()
        {
            mock.Setup(x => x.Set<Country>()).Returns(new TestDbSet<Country>
                {
                    new Country {Id = 1, Name = "USA"}, new Country {Id = 2, Name = "Germany"}
                });
            mock.Setup(x => x.Set<State>()).Returns(new TestDbSet<State>
                {
                    new State { Id = 1, Name = "Visconsin" }, new State { Id = 1, Name = "Prussia" }
                });
            mock.Setup(x => x.Set<City>()).Returns(new TestDbSet<City>
                {
                    new City { Id = 1, Name = "Washington" }, new City { Id = 1, Name = "Berlin" }
                });
            mock.Setup(x => x.Set<Place>()).Returns(new TestDbSet<Place>
                {
                    new Place { Id = 1, GooglePlaceId = "XYZ" }, new Place { Id = 1, GooglePlaceId = "123" }
                });
        }

        [TestMethod]
        public void TestGetAllCountries()
        {
            //Arrange
            SetupMock();

            // Act
            var allCountries = countryController.GetCountry();

            // Assert
            Assert.AreEqual(2, allCountries.Count());
        }

        [TestMethod]
        public void TestGetCountryWithId()
        {
            //Arrange
            SetupMock();

            // Act
            var databaseCountry = countryController.GetCountry(1);
            var country = new Country { Id = 1 };

            // Assert
            Assert.AreEqual(country.Id, databaseCountry.Queryable.SingleOrDefault().Id);
        }
        [TestMethod]
        public void TestPostCountry()
        {
            //Arrange
            SetupMock();

            // Act
            var country = new Country { Id = 3, Name = "Ukraine" };
            IHttpActionResult result = countryController.Post(country);
            var contentResult = result as StatusCodeResult;

            // Assert
            Assert.AreEqual(HttpStatusCode.NoContent, contentResult.StatusCode);
        }

        [TestMethod]
        public void TestPostCountryWithState()
        {
            //Arrange
            SetupMock();

            // Act
            var country = new Country { Id = 3, Name = "Ukraine", StateList = new List<State> { new State { Id = 1, Name = "Lviv Oblast" } } };
            IHttpActionResult result = countryController.Post(country);
            var contentResult = result as StatusCodeResult;

            // Assert
            Assert.AreEqual(HttpStatusCode.NoContent, contentResult.StatusCode);
        }

        [TestMethod]
        public void TestPostCountryWithStateAndCity()
        {
            //Arrange
            SetupMock();

            // Act
            var country = new Country
            {
                Id = 3,
                Name = "Ukraine",
                StateList = new List<State> { new State {
                        Id = 1,
                        Name = "Lviv Oblast",
                        CityList = new List<City> { new City {
                            Id =1,
                            Name = "Lviv"} } } }
            };
            IHttpActionResult result = countryController.Post(country);
            var contentResult = result as StatusCodeResult;

            // Assert
            Assert.AreEqual(HttpStatusCode.NoContent, contentResult.StatusCode);
        }

        [TestMethod]
        public void TestPostCountryWithStateAndCityAndPlace()
        {
            //Arrange
            SetupMock();

            // Act
            var country = new Country
            {
                Id = 3,
                Name = "Ukraine",
                StateList = new List<State> { new State {
                        Id = 1,
                        Name = "Lviv Oblast",
                        CityList = new List<City> { new City {
                            Id = 1,
                            Name = "Lviv", PlaceList = new List<Place>{ new Place{
                                Id = 1,
                                GooglePlaceId = "789"} } } } } }
            };
            IHttpActionResult result = countryController.Post(country);
            var contentResult = result as StatusCodeResult;

            // Assert
            Assert.AreEqual(HttpStatusCode.NoContent, contentResult.StatusCode);
        }

        [TestMethod]
        public void TestPostSameCountryWithoutStateList()
        {
            //Arrange
            SetupMock();

            // Act
            var country = new Country { Id = 3, Name = "USA" };
            IHttpActionResult result = countryController.Post(country);
            var contentResult = result as BadRequestErrorMessageResult;

            // Assert
            Assert.AreEqual("State is missing", contentResult.Message);
            Assert.IsInstanceOfType(result, typeof(BadRequestErrorMessageResult));
        }

        [TestMethod]
        public void TestPostSameCountryWithStateList()
        {
            //Arrange
            SetupMock();

            // Act
            var country = new Country { Id = 3, Name = "USA", StateList = new List<State> { new State { Id = 1, Name = "Visconsin" } } };
            IHttpActionResult result = countryController.Post(country);
            var contentResult = result as BadRequestErrorMessageResult;

            // Assert
            Assert.AreEqual("City is missing", contentResult.Message);
            Assert.IsInstanceOfType(result, typeof(BadRequestErrorMessageResult));
        }
        [TestMethod]
        public void TestPostSameCountryWithStateAndCity()
        {
            //Arrange
            SetupMock();

            // Act
            var country = new Country
            {
                Id = 3,
                Name = "USA",
                StateList = new List<State> { new State {
                        Id = 1,
                        Name = "Visconsin",
                        CityList = new List<City> { new City {
                            Id = 1,
                            Name = "Washington"} } } }
            };
            IHttpActionResult result = countryController.Post(country);
            var contentResult = result as BadRequestErrorMessageResult;

            // Assert
            Assert.AreEqual("Place is missing", contentResult.Message);
            Assert.IsInstanceOfType(result, typeof(BadRequestErrorMessageResult));
        }
        public void TestPostSameCountryWithStateAndCityAndPlace()
        {
            //Arrange
            SetupMock();

            // Act
            var country = new Country
            {
                Id = 3,
                Name = "USA",
                StateList = new List<State> { new State {
                        Id = 1,
                        Name = "Visconsin",
                        CityList = new List<City> { new City {
                            Id = 1,
                            Name = "Washington", PlaceList = new List<Place>{ new Place {
                                Id = 1,
                                GooglePlaceId = "XYZ"} } } } } }
            };
            IHttpActionResult result = countryController.Post(country);
            var contentResult = result as BadRequestErrorMessageResult;

            // Assert
            Assert.AreEqual("Place already in the database", contentResult.Message);
            Assert.IsInstanceOfType(result, typeof(BadRequestErrorMessageResult));
        }

        [TestMethod]
        public void TestDeleteCountry()
        {
            //Arrange
            SetupMock();

            // Act
            var result = countryController.Delete(1);
            var contentResult = result as StatusCodeResult;

            // Assert
            Assert.AreEqual(HttpStatusCode.NoContent, contentResult.StatusCode);
        }

        [TestMethod]
        public void TestPutNullCountry()
        {
            //Arrange
            SetupMock();

            HttpConfiguration configuration = new HttpConfiguration();
            HttpRequestMessage request = new HttpRequestMessage();
            countryController.Request = request;
            countryController.Request.Properties["MS_HttpConfiguration"] = configuration;

            // Act
            Delta<Country> delta = new Delta<Country>();
            delta.TrySetPropertyValue("Name", "Test country");
            IHttpActionResult result = countryController.Put(10, delta);
            var contentResult = result as NotFoundResult;

            // Assert
            Assert.IsInstanceOfType(contentResult, typeof(NotFoundResult));
        }
    }
}
