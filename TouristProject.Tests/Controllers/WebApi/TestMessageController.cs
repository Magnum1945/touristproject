﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.OData;
using System.Web.Http.Results;
using LetsTravel.Controllers.WebApi;
using LetsTravel.DAL.Abstractions;
using LetsTravel.Entities;
using LetsTravel.Tests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace TouristProject.Tests.Controllers.WebApi
{
    [TestClass]
    public class TestMessageController
    {
        Mock<ITouristModel> mock;
        MessageController messageController;
        public TestMessageController()
        {
            this.mock = new Mock<ITouristModel>();
            this.messageController = new MessageController(mock.Object);
        }

        public void SetupMock()
        {
            mock.Setup(x => x.Set<Conversation>()).Returns(new TestDbSet<Conversation>
                {
                    new Conversation {Id = 1, User1 = "test1@test.com", User2 = "test3@test.com"},
                    new Conversation {Id = 2, User1 = "test2@test.com", User2 = "test4@test.com"}
                });
            mock.Setup(x => x.Set<Message>()).Returns(new TestDbSet<Message>
                {
                    new Message {Id = 1, MessageText = "Test", Sender = "test1@test.com", ConversationId = 1},
                    new Message {Id = 2, MessageText = "Test", Sender = "test2@test.com", ConversationId = 2}
                });
        }

        [TestMethod]
        public void TestGetAllMessages()
        {
            //Arrange
            SetupMock();

            // Act
            var allMessages = messageController.GetMessage();

            // Assert
            Assert.AreEqual(2, allMessages.Count());
        }

        [TestMethod]
        public void TestGetMessageWithId()
        {
            //Arrange
            SetupMock();

            // Act
            var databaseMessage = messageController.GetMessage(1);
            var message = new Message { Id = 1 };

            // Assert
            Assert.AreEqual(message.Id, databaseMessage.Queryable.SingleOrDefault().Id);
        }
        [TestMethod]
        public void TestPostMessage()
        {
            //Arrange
            SetupMock();

            // Act
            var message = new Message { Id = 3, MessageText = "Test", Sender = "test1@test.com", ConversationId = 1 };
            IHttpActionResult result = messageController.Post(message);
            var contentResult = result as StatusCodeResult;

            // Assert
            Assert.AreEqual(HttpStatusCode.NoContent, contentResult.StatusCode);
        }
        [TestMethod]
        public void TestPostWrongConversationId()
        {
            //Arrange
            SetupMock();

            // Act
            var message = new Message { Id = 3, MessageText = "Test", Sender = "test1@test.com", ConversationId = 5 };
            IHttpActionResult result = messageController.Post(message);
            var contentResult = result as BadRequestErrorMessageResult;

            // Assert
            Assert.AreEqual("Conversation is missing in the database", contentResult.Message);
            Assert.IsInstanceOfType(result, typeof(BadRequestErrorMessageResult));
        }

        [TestMethod]
        public void TestPostWrongSender()
        {
            //Arrange
            SetupMock();

            // Act
            //var message = new Message { Id = 3, MessageText = "Test", Sender = "test5@test.com", ConversationId = 1 };
            var message = new Message { Id = 3, Sender = "test5@test.com", ConversationId = 1 };
            IHttpActionResult result = messageController.Post(message);
            var contentResult = result as BadRequestErrorMessageResult;

            // Assert
            Assert.AreEqual("Wrong sender", contentResult.Message);
            Assert.IsInstanceOfType(result, typeof(BadRequestErrorMessageResult));
        }

        [TestMethod]
        public void TestDeleteMessage()
        {
            //Arrange
            SetupMock();

            // Act
            var result = messageController.Delete(1);
            var contentResult = result as StatusCodeResult;

            // Assert
            Assert.AreEqual(HttpStatusCode.NoContent, contentResult.StatusCode);
        }

        [TestMethod]
        public void TestPutNullMessage()
        {
            //Arrange
            SetupMock();

            HttpConfiguration configuration = new HttpConfiguration();
            HttpRequestMessage request = new HttpRequestMessage();
            messageController.Request = request;
            messageController.Request.Properties["MS_HttpConfiguration"] = configuration;

            // Act
            Delta<Message> delta = new Delta<Message>();
            delta.TrySetPropertyValue("User1", "Test user");
            IHttpActionResult result = messageController.Put(10, delta);
            var contentResult = result as NotFoundResult;

            // Assert
            Assert.IsInstanceOfType(contentResult, typeof(NotFoundResult));
        }
    }
}
