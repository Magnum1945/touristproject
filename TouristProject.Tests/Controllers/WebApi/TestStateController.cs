﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.OData;
using System.Web.Http.Results;
using LetsTravel.Controllers.WebApi;
using LetsTravel.DAL.Abstractions;
using LetsTravel.Entities;
using LetsTravel.Tests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace TouristProject.Tests.Controllers.WebApi
{
    [TestClass]
    public class TestStateController
    {
        Mock<ITouristModel> mock;
        StateController stateController;
        public TestStateController()
        {
            this.mock = new Mock<ITouristModel>();
            this.stateController = new StateController(mock.Object);
        }

        public void SetupMock()
        {
            mock.Setup(x => x.Set<State>()).Returns(new TestDbSet<State>
                {
                    new State {Id = 1, Name = "Utar Pradesh"},
                    new State {Id = 2, Name = "Okawango"}
                });
        }

        [TestMethod]
        public void TestGetAllStates()
        {
            //Arrange
            SetupMock();

            // Act
            var allStates = stateController.GetState();

            // Assert
            Assert.AreEqual(2, allStates.Count());
        }

        [TestMethod]
        public void TestGetStateWithId()
        {
            //Arrange
            SetupMock();

            // Act
            var databaseState = stateController.GetState(1);
            var state = new State { Id = 1 };

            // Assert
            Assert.AreEqual(state.Id, databaseState.Queryable.SingleOrDefault().Id);
        }
        [TestMethod]
        public void TestPostState()
        {
            //Arrange
            SetupMock();

            // Act
            var state = new State { Id = 3, Name = "Lviv Oblast" };
            IHttpActionResult result = stateController.Post(state);
            var contentResult = result as StatusCodeResult;

            // Assert
            Assert.AreEqual(HttpStatusCode.NoContent, contentResult.StatusCode);
        }
        [TestMethod]
        public void TestPostSameState()
        {
            //Arrange
            SetupMock();

            // Act
            var state = new State { Id = 3, Name = "Okawango" };
            IHttpActionResult result = stateController.Post(state);
            var contentResult = result as BadRequestErrorMessageResult;

            // Assert
            Assert.AreEqual("State already in the database", contentResult.Message);
            Assert.IsInstanceOfType(result, typeof(BadRequestErrorMessageResult));
        }

        [TestMethod]
        public void TestDeleteState()
        {
            //Arrange
            SetupMock();

            // Act
            var result = stateController.Delete(1);
            var contentResult = result as StatusCodeResult;

            // Assert
            Assert.AreEqual(HttpStatusCode.NoContent, contentResult.StatusCode);
        }

        [TestMethod]
        public void TestPutNullState()
        {
            //Arrange
            SetupMock();

            HttpConfiguration configuration = new HttpConfiguration();
            HttpRequestMessage request = new HttpRequestMessage();
            stateController.Request = request;
            stateController.Request.Properties["MS_HttpConfiguration"] = configuration;

            // Act
            Delta<State> delta = new Delta<State>();
            delta.TrySetPropertyValue("Name", "Test state");
            IHttpActionResult result = stateController.Put(10, delta);
            var contentResult = result as NotFoundResult;

            // Assert
            Assert.IsInstanceOfType(contentResult, typeof(NotFoundResult));
        }
    }
}
