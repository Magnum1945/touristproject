﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LetsTravel.DAL.Abstractions;
using Moq;
using LetsTravel.Controllers.MvcApp;
using LetsTravel.Helpers;
using System.Web.Mvc;
using LetsTravel.Entities;
using LetsTravel.Tests;
using System.Security.Principal;
using System.Threading;
using System.Web;
using System.Security.Claims;
using LetsTravel.Entities.ViewModels;

namespace TouristProject.Tests.Controllers.MvcApp
{
    [TestClass]
    public class TestTripController
    {
        Mock<ITouristModel> touristMock;
        Mock<IOpenWeather> weatherMock;
        TripController tripController;
        public TestTripController()
        {
            this.touristMock = new Mock<ITouristModel>();
            this.weatherMock = new Mock<IOpenWeather>();
            this.tripController = new TripController(touristMock.Object, weatherMock.Object);
        }

        public void SetupMock()
        {
            touristMock.Setup(x => x.Set<Country>()).Returns(new TestDbSet<Country>
                {
                    new Country {Id = 1, Name = "USA"}, new Country {Id = 2, Name = "Germany"}
                });
            touristMock.Setup(x => x.Set<State>()).Returns(new TestDbSet<State>
                {
                    new State { Id = 1, Name = "Visconsin", CountryId = 1 }, new State { Id = 2, Name = "Prussia", CountryId = 2 }
                });
            touristMock.Setup(x => x.Set<City>()).Returns(new TestDbSet<City>
                {
                    new City { Id = 1, Name = "Washington", StateId = 1 }, new City { Id = 1, Name = "Berlin", StateId = 2 }
                });
            touristMock.Setup(x => x.Set<Trip>()).Returns(new TestDbSet<Trip>
                {
                    new Trip {Id = 1, UserLogin = "test1@test.com"}, new Trip {Id = 2, UserLogin = "test2@test.com"}
                });
            touristMock.Setup(x => x.Set<Place>()).Returns(new TestDbSet<Place>
                {
                    new Place { Id = 1, GooglePlaceId = "XYZ", DurationInSeconds = 150, CityId = 1 }, new Place { Id = 2, GooglePlaceId = "123", DurationInSeconds = 250, CityId = 1 }
                });
        }

        [TestMethod]
        public void TestShowPlaceWithId()
        {
            //Arrange
            SetupMock();

            // Act
            var result = tripController.ShowPlace(1) as ViewResult;
            var place = (Place)result.ViewData.Model;

            // Assert
            Assert.AreEqual(1, place.Id);
        }

        [TestMethod]
        public void TestSubmitCreatedTrip()
        {
            //Arrange
            SetupMock();

            // Act
            string id = "1,2";
            string duration = "100,200";
            string distance = "400,500";

            var fakeHttpContext = new Mock<HttpContextBase>();
            var fakeIdentity = new GenericIdentity("User");
            var principal = new GenericPrincipal(fakeIdentity, null);
            var session = new Mock<HttpSessionStateBase>();

            session.Setup(t=>t["CreatedTrip"]).Returns(new TripViewModel { CityName = "ABC", UserLogin = "User"});
            fakeHttpContext.Setup(t => t.Session).Returns(session.Object);
            fakeHttpContext.Setup(t => t.User).Returns(principal);
            var controllerContext = new Mock<ControllerContext>();
            controllerContext.Setup(t => t.HttpContext).Returns(fakeHttpContext.Object);
            tripController.ControllerContext = controllerContext.Object;

            tripController.SubmitCreatedTrip(id, duration, distance);
            var result = tripController.HttpContext.Session["CreatedTrip"] as TripViewModel;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual("User", result.UserLogin);
        }

        [TestMethod]
        public void TestSaveCreatedTrip()
        {
            //Arrange
            SetupMock();
            TripViewModel trip = new TripViewModel
            {
                CountryName = "USA",
                StateName = "Visconsin",
                CityName = "Washington",
                UserLogin = "User",
                Description = "Test description",
                Distance = 200,
                Duration = new TimeSpan(08,05,01),
                StartTime = new DateTime(2015,08,12),
                PlaceList = new List<Place> { new Place { Id = 1}, new Place { Id = 2} },
                TripName = "Test name",
                TripPrice = "123",
            };

            // Act
            var result = tripController.SaveCreatedTrip(trip) as ViewResult;

            // Assert
            Assert.AreEqual("Details", result.ViewName);
        }
    }
}
